/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package phat.mobile.servicemanager;

import org.junit.*;
import static org.junit.Assert.*;
import phat.mobile.servicemanager.services.Service;
import phat.mobile.servicemanager.services.ServiceImpl;
/**
 *
 * @author Pablo
 */
public class ServiceManagerTest {

    public ServiceManagerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
        
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Register a service and ask for him.
     * The test checks if the info is ok.
     */
    @Test
    public void testRegisterOneService() {
        ServiceManager serviceManager = new ServiceManagerImpl();
	serviceManager.registerService("a1", new ServiceImpl(Service.MICROPHONE, "localhost", 3000));
        Service service = serviceManager.getService("a1", Service.MICROPHONE);
        assertEquals(service.getType(), Service.MICROPHONE);
        assertEquals(service.getIp(), "localhost");
        assertEquals(service.getPort(), 3000);
    }
    
    /**
     * Register tree services and ask for them.
     * The test checks if the info is ok.
     */
    @Test
    public void testRegisterSeveralService() {
        ServiceManager serviceManager = new ServiceManagerImpl();
	serviceManager.registerService("a1", new ServiceImpl(Service.MICROPHONE, "localhost1", 3001));
        serviceManager.registerService("a1", new ServiceImpl("Accelerometer", "localhost2", 3002));
        serviceManager.registerService("a1", new ServiceImpl("Pressure", "localhost3", 3003));
        
        Service service = serviceManager.getService("a1", Service.MICROPHONE);
        assertEquals(service.getType(), Service.MICROPHONE);
        assertEquals(service.getIp(), "localhost1");
        assertEquals(service.getPort(), 3001);
        
        service = serviceManager.getService("a1", "Accelerometer");
        assertEquals(service.getType(), "Accelerometer");
        assertEquals(service.getIp(), "localhost2");
        assertEquals(service.getPort(), 3002);
        
        service = serviceManager.getService("a1", "Pressure");
        assertEquals(service.getType(), "Pressure");
        assertEquals(service.getIp(), "localhost3");
        assertEquals(service.getPort(), 3003);
    }
}
