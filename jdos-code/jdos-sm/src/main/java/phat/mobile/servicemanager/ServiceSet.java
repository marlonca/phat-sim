package phat.mobile.servicemanager;

import java.util.List;

import phat.mobile.servicemanager.services.Service;

public interface ServiceSet {

    public String getId();

    public List<Service> getServices();

    public void add(Service service);

    public void remove(Service service);

    public Service get(String serviceTypeName);
}
