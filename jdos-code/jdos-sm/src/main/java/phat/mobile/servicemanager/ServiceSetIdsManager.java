package phat.mobile.servicemanager;

import java.util.HashMap;
import java.util.Map;

public class ServiceSetIdsManager {

    private ServiceManager serviceManager;
    // Almacena por cada serviceSetId, true si esta asignado y false si no.
    private Map<String, Boolean> assignments;

    public ServiceSetIdsManager(ServiceManager serviceManager) {
        super();
        this.serviceManager = serviceManager;
        this.assignments = new HashMap<String, Boolean>();
    }

    public String nextServiceSetId() {
        updateServiceSetIds();
        for (String ssi : assignments.keySet()) {
            if (!assignments.get(ssi)) {
                return ssi;
            }
        }
        return null;
    }

    private void updateServiceSetIds() {
        for (String ssi : serviceManager.getServiceSetIds()) {
            Boolean b = assignments.get(ssi);
            if (b == null) {
                assignments.put(ssi, false);
            }
        }
    }
}
