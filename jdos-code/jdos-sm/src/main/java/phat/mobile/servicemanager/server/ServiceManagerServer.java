package phat.mobile.servicemanager.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import phat.mobile.servicemanager.CommonVars;
import phat.mobile.servicemanager.ServiceManager;
import phat.mobile.servicemanager.ServiceManagerImpl;
import phat.mobile.servicemanager.messages.ErrorMessageImpl;
import phat.mobile.servicemanager.messages.RequestService;
import phat.mobile.servicemanager.services.Service;

public class ServiceManagerServer implements Runnable {

    private static ServiceManagerServer serviceManagerServer = null;

    public static ServiceManagerServer getInstance() {
        if (serviceManagerServer == null) {
            serviceManagerServer = new ServiceManagerServer();
        }
        return serviceManagerServer;
    }
    private ServiceManager serviceManager;
    // Contador de puertos que van solicitando los servidores de recursos (microfono, camara)
    // de cada smartphone.
    private int nextPort = 60000;
    private ServerSocket serverSocket;
    private int defaultPort = CommonVars.SERVICE_MANAGER_SERVER_PORT;
    private Thread serverThread;
    private boolean running = false;

    private ServiceManagerServer() {
        serviceManager = new ServiceManagerImpl();
        serverThread = new Thread(this);
        serverThread.start();
    }

    public int getNextPort() {
        return nextPort++;
    }

    public String getIP() {
        if (serverSocket != null) {
            return serverSocket.getInetAddress().getHostAddress();
        }
        return null;
    }

    private InetAddress getAddress() {
        try {
            for (Enumeration e = NetworkInterface.getNetworkInterfaces();
                    e.hasMoreElements();) {

                NetworkInterface ni = (NetworkInterface) e.nextElement();
                if (ni.getName().contains("eth") || ni.getName().contains("wlan")) {
                    for (Enumeration ee = ni.getInetAddresses(); ee.hasMoreElements();) {
                        InetAddress ip = (InetAddress) ee.nextElement();
                        if (ip instanceof Inet4Address) {
                            return ip;
                        }
                        //System.out.println("Ip's: " + ip.getHostAddress());
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Error");
        }
        return null;
    }

    public synchronized void setServiceManager(ServiceManager sm) {
        serviceManager = sm;
    }

    public synchronized ServiceManager getServiceManager() {
        return serviceManager;
    }

    public synchronized void stop() {
        running = false;
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException ex) {
            }
        }
    }

    public void run() {
        running = true;
        InetAddress ip = getAddress();
        try {
            System.out.println("ServerSocket!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            serverSocket = new ServerSocket(defaultPort, 0, ip);
        } catch (IOException ex) {
            Logger.getLogger(ServiceManagerServer.class.getName()).log(Level.SEVERE, "ServerSocket", ex);
            running = false;
        }
        while (running) {
            try {
                try {
                    Socket socket = serverSocket.accept();
                    if (socket != null) {
                        ManageServiceRequest msr = new ManageServiceRequest(socket);
                        msr.start();
                    }
                } catch (SocketException ex) {
                    if (running) {
                        Logger.getLogger(ServiceManagerServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(ServiceManagerServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(ServiceManagerServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public class ManageServiceRequest extends Thread {

        private Socket socket;
        private ObjectInputStream ois;
        private ObjectOutputStream oos;

        public ManageServiceRequest(Socket socket) {
            System.out.println("Nuevo cliente: " + socket);
            this.socket = socket;
            try {
                ois = new ObjectInputStream(socket.getInputStream());
                oos = new ObjectOutputStream(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /*
         * Recives a require message asking for a service type (e.g. microphone)
         * of a set of services (from a simulated device).
         * 
         * @see es.um.servicemanager.services.Service
         * @see java.lang.Thread#run()
         */
        @Override
        public void run() {
            RequestService rs;
            try {
                rs = (RequestService) ois.readObject();
                System.out.println("Request: " + rs.getServiceSetId() + ", " + rs.getServiceTypeName());
                Service service = serviceManager.getService(rs.getServiceSetId(), rs.getServiceTypeName());
                System.out.println("Service: " + service);
                if (service != null) {
                    try {
                        oos.writeObject(service);
                        oos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        oos.writeObject(new ErrorMessageImpl("Service doesn't exists!"));
                        oos.flush();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                ois.close();
                oos.close();
                socket.close();
            } catch (IOException ex) {
                Logger.getLogger(ServiceManagerServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
