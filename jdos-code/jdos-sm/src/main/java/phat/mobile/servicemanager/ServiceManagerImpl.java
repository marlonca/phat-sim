package phat.mobile.servicemanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import phat.mobile.servicemanager.services.Service;

public class ServiceManagerImpl implements ServiceManager {

    Map<String, ServiceSet> serviceSets;

    public ServiceManagerImpl() {
        serviceSets = new HashMap<String, ServiceSet>();
    }

    @Override
    public synchronized void registerService(String serviceSetId, Service service) {
        ServiceSet ss = serviceSets.get(serviceSetId);
        if (ss == null) {
            ss = new ServiceSetImpl(serviceSetId);
        }
        ss.add(service);
        serviceSets.put(serviceSetId, ss);
    }

    @Override
    public synchronized Service getService(String serviceSetId, String serviceTypeName) {
        ServiceSet ss = serviceSets.get(serviceSetId);
        if (ss == null) {
            return null;
        }
        return ss.get(serviceTypeName);
    }

    @Override
    public List<String> getServiceSetIds() {
        return new ArrayList<String>(serviceSets.keySet());
    }
}
