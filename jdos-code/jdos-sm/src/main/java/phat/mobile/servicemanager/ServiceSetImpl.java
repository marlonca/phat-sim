package phat.mobile.servicemanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import phat.mobile.servicemanager.services.Service;

/**
 * Clase que representa un conjunto de servicios, almacenando uno de cada tipo
 *
 * @author pablo
 *
 */
public class ServiceSetImpl implements ServiceSet {

    // <Service type, Service>
    private Map<String, Service> services;
    private String id;

    public ServiceSetImpl(String id) {
        this.id = id;
        this.services = new HashMap<String, Service>();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public List<Service> getServices() {
        return new ArrayList<Service>(services.values());
    }

    @Override
    public void add(Service service) {
        System.out.println("add " + service);
        services.put(service.getType(), service);
    }

    @Override
    public void remove(Service service) {
        services.remove(service.getType());
    }

    @Override
    public Service get(String serviceTypeName) {
        return services.get(serviceTypeName);
    }
}
