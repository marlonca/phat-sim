package phat.mobile.servicemanager.messages;

import phat.mobile.servicemanager.services.Service;

public interface RegisterServiceMessage {

    public Service getService();

    public String getServiceSetId();
}
