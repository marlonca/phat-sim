package phat.mobile.servicemanager.messages;

public class ServiceSetIdResponse implements ServiceManagerMessage {

    private String serviceSetId;

    public ServiceSetIdResponse(String serviceSetId) {
        this.serviceSetId = serviceSetId;
    }

    public String getServiceSetId() {
        return serviceSetId;
    }
}
