package phat.mobile.servicemanager.messages;

import java.io.Serializable;

public interface RequestService extends Serializable {

    public String getServiceSetId();

    public String getServiceTypeName();
}
