package phat.mobile.servicemanager.services;

import java.io.Serializable;

/**
 * Interface which contains the information to connect to a service.
 *
 * @author pablo
 */
public interface Service extends Serializable {

    public static final String MICROPHONE = "microphone";
    public static final String CAMERA = "camera";
    public static final String ACCELEROMETER = "accelerometer";
    public static final String PRESENCE = "presence";
    public static final String DOOR = "door";
    public static final String PRESSURE = "pressure";
    public static final String TEMPERATURE = "temperature";
    public static final String HUMIDITY = "humidity";
    public static final String RFID = "rfid";

    public String getType();

    public String getIp();

    public int getPort();
}
