package phat.mobile.servicemanager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class CommonVars {
	public static final int SERVICE_MANAGER_SERVER_PORT = 65056;

	// File name which is copied in the emulator /sdcard/
        public static final String VAPI_FILE = "vapi.props";

	// Propiedades del fichero VAPI_FILE

	// IP y puerto del ServiceMangerServer
	public static final String PROP_SMS_IP = "sms_ip";
	public static final String PROP_SMS_PORT = "sms_port";
	// Nombre del móvil en el simulador y que se usa para identificar
	// el grupo de servicios (serviceSetId) en ServiceManager.
	public static final String PROP_DEVICE_NAME = "deviceName";

	private static Properties properties;

        // get properties saved in VAPI_FILE
	public static Properties getPorperties() {
		System.out.println("getPorperties()");
		if (properties == null) {
			properties = new Properties();
			System.out.println("File = "+"/sdcard/" + VAPI_FILE);
			File file = new File("/sdcard/" + VAPI_FILE);
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			try {
				System.out.println("Load Props");
				properties.load(fis);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		return properties;
	}
}
