package phat.mobile.servicemanager.messages;

public interface ErrorMessage extends ServiceManagerMessage {

    public String getMessage();
}
