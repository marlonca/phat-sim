package phat.mobile.servicemanager.messages;

public class ErrorMessageImpl implements ErrorMessage {

    private String message;

    public ErrorMessageImpl(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
