package phat.mobile.servicemanager.messages;

import phat.mobile.servicemanager.services.Service;

public class RegisterServiceMessageImpl implements RegisterServiceMessage {

    private Service service;
    private String serviceSetId;

    public RegisterServiceMessageImpl(String serviceSetId, Service service) {
        super();
        this.service = service;
        this.serviceSetId = serviceSetId;
    }

    @Override
    public Service getService() {
        return service;
    }

    @Override
    public String getServiceSetId() {
        return serviceSetId;
    }
}
