package phat.mobile.servicemanager;

import java.util.List;
import phat.mobile.servicemanager.services.Service;

/**
 * Interface to register and get services. Services are grouped by an id and
 * every service has a type.
 *
 * @author pablo
 */
public interface ServiceManager {

    public void registerService(String serviceSetId, Service service);

    public Service getService(String serviceSetId, String serviceTypeName);

    public List<String> getServiceSetIds();
}
