package phat.mobile.servicemanager.services;

public class ServiceImpl implements Service {

    private String type;
    private String ip;
    private int port;

    public ServiceImpl(String type, String ip, int port) {
        super();
        this.type = type;
        this.ip = ip;
        this.port = port;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getIp() {
        return ip;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "type=" + type + ",ip:port=" + ip + ":" + port;
    }
}
