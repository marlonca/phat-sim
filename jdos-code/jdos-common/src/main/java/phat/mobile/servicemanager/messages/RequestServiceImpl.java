package phat.mobile.servicemanager.messages;

public class RequestServiceImpl implements RequestService {

    private String serviceSetId;
    private String serviceTypeName;

    public RequestServiceImpl(String serviceSetId, String serviceTypeName) {
        super();
        this.serviceSetId = serviceSetId;
        this.serviceTypeName = serviceTypeName;
    }

    @Override
    public String getServiceSetId() {
        return serviceSetId;
    }

    @Override
    public String getServiceTypeName() {
        return serviceTypeName;
    }
}
