package phat.mobile.servicemanager.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Properties;
import phat.mobile.servicemanager.CommonVars;
import phat.mobile.servicemanager.ServiceManager;
import phat.mobile.servicemanager.messages.*;
import phat.mobile.servicemanager.services.Service;

/**
 * Proxy that implements the ServiceManager methods. Every method recalls to a
 * ServiceManager server running in a given ip and port.
 */
public class ServiceManagerRemote implements ServiceManager {

    private int port;
    private String ip;
    private String mobileName;
    private static ServiceManagerRemote serviceManagerRemote;

    public static ServiceManagerRemote getInstance() {
        if (serviceManagerRemote == null) {
            serviceManagerRemote = new ServiceManagerRemote();
        }
        return serviceManagerRemote;
    }

    private ServiceManagerRemote() {
        System.out.println("ServiceManagerRemote()");
        Properties prop = CommonVars.getPorperties();
        System.out.println("prop = " + prop);
        if (prop != null) {
            ip = (String) prop.get(CommonVars.PROP_SMS_IP);
            port = Integer.parseInt((String) prop.get(CommonVars.PROP_SMS_PORT));
            mobileName = (String) prop.get(CommonVars.PROP_DEVICE_NAME);
            System.out.println("ServiceManagerRemote(" + mobileName + "," + ip + ":" + port);
        }
    }

    public ServiceManagerRemote(String deviceName, String ip, int port) {
        this.mobileName = deviceName;
        this.ip = ip;
        this.port = port;
    }
    
    @Override
    public void registerService(String serviceSetId, Service service) {
        Socket socket = null;
        try {
            socket = new Socket(ip, port);
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            RegisterServiceMessage rsm = new RegisterServiceMessageImpl(mobileName, service);
            oos.writeObject(rsm);
            oos.flush();
            oos.close();
            socket.close();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Service getService(String serviceSetId, String serviceTypeName) {
        Socket socket = null;
        try {
            System.out.println("getService(" + serviceSetId + "," + serviceTypeName + ")");
            System.out.println("new Socket(" + ip + "," + port + ")");
            socket = new Socket(ip, port);
            System.out.println("socket = " + socket);
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("oos = " + socket);
            RequestService rs = new RequestServiceImpl(mobileName, serviceTypeName);
            System.out.println("RequestService(" + mobileName + "," + serviceTypeName + ") " + rs);
            oos.writeObject(rs);
            System.out.println("write rs");
            oos.flush();
            System.out.println("flush");
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            System.out.println("ois");
            Object answer = ois.readObject();
            System.out.println("answer");
            if (answer instanceof ErrorMessage) {
                ErrorMessage em = (ErrorMessage) answer;
                System.out.println("Error: " + em.getMessage());
            } else if (answer instanceof Service) {
                Service service = (Service) answer;
                System.out.println("Service gotten: " + service.getIp() + ":"
                        + service.getPort() + ", type = " + service.getType());
                return service;
            }
            ois.close();
            oos.close();
            socket.close();
        } catch (UnknownHostException e) {
            System.out.println("UnknownHostException");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IOException");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("ClassNotFoundException");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getServiceSetIds() {
        return null;
    }
}
