package sim.android.app;

import android.os.Build;
import sim.android.hardware.SensorManager;

public class Activity extends android.app.Activity {
	
	@Override
    public Object getSystemService(String name) {
        if (getBaseContext() == null) {
            throw new IllegalStateException(
                    "System services not available to Activities before onCreate()");
        }
     // by a-abiri and Ted Hopp
    	// from http://stackoverflow.com/questions/6864251/how-to-find-out-from-code-if-my-android-app-runs-on-emulator-or-real-device
        boolean inEmulator = "generic".equals(Build.BRAND.toLowerCase());
        if(name.equals(SENSOR_SERVICE) && inEmulator) {                
        	 return new SensorManager();
        }
        return super.getSystemService(name);
    }
}
