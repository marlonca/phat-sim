package sim.android.app.state;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import sim.android.app.state.message.ActivityStateMessage;
import sim.android.hardware.service.SimSensorEvent;
import sim.android.hardware.service.impl.ParamServices;
import android.util.Log;

public class ActivityStateServiceImpl {
	private static String ipSocketUbikSim = ParamServices.getProperty(ParamServices.IP_SOCKET_UBIKSIM);

	private static ActivityStateServiceImpl assi = null;
	
	protected Socket s;
	protected ObjectOutputStream oos;
	
	private ActivityStateServiceImpl() {
	}

	public static ActivityStateServiceImpl getInstance() {
		if(assi == null)
			assi = new ActivityStateServiceImpl();
		return assi;
	}
	
	private void connect() {
		if (ipSocketUbikSim != null) {
			try {
				if (s == null)
					s = new Socket(ipSocketUbikSim, SimSensorEvent.PORT);
				if (oos == null)
					oos = new ObjectOutputStream(s.getOutputStream());
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				s = null;
				System.out
						.println("AndroidMobile: No accesible: 192.168.0.195:6450");
				return;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				oos = null;
				System.out
						.println("AndroidMobile: Problemas obteniendo InputStream");
			}
		}
	}

	public void notifyStateChanged(ActivityStateMessage as) {
		if (s == null || oos == null) {
			connect();
		}
		if (ipSocketUbikSim != null && s != null && s.isConnected()
				&& oos != null) {
			try {
				if (as != null) {
					oos.writeObject(as);
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				Log.d(getClass().getSimpleName(), "IOException",
						e1.getCause());
				s = null;
				oos = null;
			}
		}
	}
		
	public void stop() {
		try {
			if (oos != null)
				oos.close();
			if (s != null)
				s.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void start() {
		if (s == null || oos == null) {
			connect();
		}
	}
}