package sim.android;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;



public class SensorManagerWrapper {

	private android.hardware.SensorManager realSensor=null;
	private sim.android.hardware.SensorManager simSensor=null;

	
	public SensorManagerWrapper(boolean inEmulator, Activity act){
		if (inEmulator)
			simSensor=new  sim.android.hardware.SensorManager();
		else
			realSensor=(SensorManager) act.getSystemService(Activity.SENSOR_SERVICE);
	}
	
	public Sensor getDefaultSensor(int typeAccelerometer) {
		if (simSensor!=null)
			return simSensor.getDefaultSensor(typeAccelerometer);
		else
			return realSensor.getDefaultSensor(typeAccelerometer);
	}

	public void registerListener(android.hardware.SensorEventListener listener,
			android.hardware.Sensor mAccelerometer, int sensorDelayNormal) {
		if (simSensor!=null)
			 simSensor.registerListener(listener,mAccelerometer,sensorDelayNormal);
		else
			 realSensor.registerListener(listener,mAccelerometer,sensorDelayNormal);
	}

	public void unregisterListener(android.hardware.SensorEventListener listener) {
		if (simSensor!=null)
			 simSensor.unregisterListener(listener);
		else
			realSensor.unregisterListener(listener);
		
	}

	

}
