package sim.android.hardware.service.impl;

import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.UnknownHostException;

import sim.android.hardware.SensorManager;
import sim.android.hardware.SensorManager.EventHandler;
import sim.android.hardware.service.SimSensorEvent;
import android.util.Log;
import phat.mobile.servicemanager.client.ServiceManagerRemote;
import phat.mobile.servicemanager.services.Service;

public class SensorEventService implements Runnable {

    protected EventHandler eventHandler;
    protected Socket s;
    protected ObjectInputStream oin;
    private Thread thread;
    private SensorManager mSensorManager;
    Service sensorService;

    public SensorEventService(SensorManager sensorManager, EventHandler eventHandler) {
        this.mSensorManager = sensorManager;
        this.eventHandler = eventHandler;
    }

    private void connect() {
        if (sensorService != null) {
            return;
        }
        Log.d(getClass().getName(), "ServiceManagerRemote().getInstance()");
        ServiceManagerRemote smr = ServiceManagerRemote.getInstance();
        Log.d(getClass().getName(), "ServiceManagerRemote: " + smr);
        sensorService = smr.getService(null, Service.ACCELEROMETER);
        Log.d(getClass().getName(), "Service: " + sensorService);
        if (sensorService != null) {
            try {
                if (s == null) {
                    Log.d(getClass().getName(), "New socket: " + sensorService.getIp() + ":" + sensorService.getPort());
                    s = new Socket(sensorService.getIp(), sensorService.getPort());
                }
                if (oin == null) {
                    oin = new ObjectInputStream(s.getInputStream());
                }
                System.out.println("AndroidMobile: InputStream obtenido");

            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                s = null;
                return;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                oin = null;
                System.out.println("AndroidMobile: Problemas obteniendo InputStream");
            }
        }
    }

    public void run() {
        do {
            if (s == null || oin == null) {
                connect();
            }
            /*
             * try { sleep(wait); } catch (InterruptedException e) { return; }
             */
            if (s != null && s.isConnected() && oin != null) {
                try {
                    SimSensorEvent sse = (SimSensorEvent) oin.readObject();
                    if (sse != null) {
                    	
                    	
                        Constructor<?> eventConstructor = android.hardware.SensorEvent.class.getDeclaredConstructors()[0];
                         eventConstructor.setAccessible(true);
                        
                        android.hardware.SensorEvent event=(android.hardware.SensorEvent) eventConstructor.newInstance(sse.getValues().length);
                        if (sse.getType() == SimSensorEvent.TYPE_ACCELEROMETER) {
                            Log.d("SensorEventService", sse.toString());
                        }
                        if (sse.getType() == SimSensorEvent.TYPE_MAGNETIC_FIELD) {
                            Log.d("SensorEventService", sse.toString());
                        }
                        if (sse.getType() == SimSensorEvent.TYPE_ORIENTATION) {
                            Log.d("SensorEventService", sse.toString());
                        }
                        for (int k=0;k<sse.getValues().length;k++)
                        	event.values[k]=sse.getValues()[k];
                        event.timestamp = System.currentTimeMillis();
                        event.accuracy = sse.getAccuracy();
                        event.sensor = mSensorManager.getSensorList(sse.getType()).get(0);                        
                        notifySensorValue(event);
                    }
                } catch (ClassNotFoundException e1) {
                    // TODO Auto-generated catch block
                    Log.d(getClass().getSimpleName(), "ClassNotFoundException",
                            e1.getException());

                    s = null;
                    oin = null;
                    break;
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    Log.d(getClass().getSimpleName(), "IOException",
                            e1.getCause());
                    s = null;
                    oin = null;
                    break;
                } catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            } else {
                break;
            }
        } while (true);
    }

    private void notifySensorValue(android.hardware.SensorEvent sensorEvent) {
        Message m;
        m = eventHandler.obtainMessage(EventHandler.NEW_VALUE, 0, 0, sensorEvent);
        eventHandler.sendMessage(m);
    }
    
    public void stop() {
        thread.interrupt();
        try {
            if (oin != null) {
                oin.close();
            }
            if (s != null) {
                s.close();
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    public void start() {
        thread = new Thread(this);
        thread.start();
    }
}
