package sim.android.hardware;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.junit.runners.Parameterized.Parameter;

import sim.android.hardware.service.CameraCaptureImageService;
import sim.android.hardware.service.impl.CameraCaptureImageServiceImpl;
import android.util.Log;
import android.view.SurfaceHolder;
import android.graphics.ImageFormat;
import android.hardware.Camera.Parameters;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import sim.android.hardware.service.CameraImageCapture;

/*
 import com.prosyst.mbs.rsm.IRsmBondService;
 import com.prosyst.mbs.rsm.IRsmRegistry;
 import com.prosyst.mbs.rsm.ParameterValue;
 */
/**
 * This class aim to interact with the simulator.
 *
 * @author pablo
 *
 */
public class Camera {

    private static final String TAG = "Camera";
    // These match the enums in frameworks/base/include/camera/Camera.h
    private static final int CAMERA_MSG_ERROR = 0x001;
    private static final int CAMERA_MSG_SHUTTER = 0x002;
    private static final int CAMERA_MSG_FOCUS = 0x004;
    private static final int CAMERA_MSG_ZOOM = 0x008;
    private static final int CAMERA_MSG_PREVIEW_FRAME = 0x010;
    private static final int CAMERA_MSG_VIDEO_FRAME = 0x020;
    private static final int CAMERA_MSG_POSTVIEW_FRAME = 0x040;
    private static final int CAMERA_MSG_RAW_IMAGE = 0x080;
    private static final int CAMERA_MSG_COMPRESSED_IMAGE = 0x100;
    private static final int CAMERA_MSG_ALL_MSGS = 0x1FF;
    private static final int SET_IMAGE = 0xFFFF; // Extra
    private EventHandler mEventHandler;
    private ShutterCallback mShutterCallback;
    private PictureCallback mRawImageCallback;
    private PictureCallback mJpegCallback;
    private PreviewCallback mPreviewCallback;
    private PictureCallback mPostviewCallback;
    private AutoFocusCallback mAutoFocusCallback;
    private OnZoomChangeListener mZoomListener;
    private ErrorCallback mErrorCallback;
    private boolean mOneShot;
    private boolean mWithBuffer;
    private static Camera mcamera;
    private SurfaceHolder mHolder;
    private CameraCaptureImageService cameraCaptureImageService;

    /*
     * private IRsmRegistry registry; private IRsmBondService rService;
     */

    /*
     * ServiceConnection conn = new ServiceConnection() { public void
     * onServiceConnected(ComponentName className, IBinder service) { // Get the
     * IRsmRegistry service //registry = IRsmRegistry.Stub.asInterface(service);
     * }
     * 
     * public void onServiceDisconnected(ComponentName arg0) { // TODO
     * Auto-generated method stub image = null; rService = null; registry =
     * null; } };
     */
    public static int getNumberOfCameras() {
        return 1;
    }

    public static void getCameraInfo(int cameraId, CameraInfo cameraInfo) {
        cameraInfo.facing = CameraInfo.CAMERA_FACING_BACK;
        cameraInfo.orientation = 0;
    }

    public static class CameraInfo {

        public static final int CAMERA_FACING_BACK = 0;
        public static final int CAMERA_FACING_FRONT = 1;
        public int facing;
        public int orientation;
    };

    public static Camera open(int cameraId) {
        if (mcamera == null) {
            mcamera = new Camera(cameraId);
        }
       // mcamera.getParameters().set(Parameters.KEY_PREVIEW_FORMAT, Parameters.PIXEL_FORMAT_YUV420SP);
        //mcamera.getParameters().setPreviewSize(800, 480);
        // TODO inform to simulator that the real mobile will use the camera
		/*
         * String actionName= "sim.android.app.intent.action.SIMULATION"; Intent
         * intent = new Intent(actionName); intent.getExtras().putString("URL",
         * url); intent.getExtras().putString("TYPE", "CAMERA_FRAME");
         * activity.startActivity(intent);
         */
        return mcamera;
    }

    public static Camera open() {
        /*
         * int numberOfCameras = getNumberOfCameras(); CameraInfo cameraInfo =
         * new CameraInfo(); for (int i = 0; i < numberOfCameras; i++) {
         * getCameraInfo(i, cameraInfo); if (cameraInfo.facing ==
         * CameraInfo.CAMERA_FACING_BACK) { return new Camera(i); } }
         */
        return open(0);
    }

    Camera(int cameraId) {
        Log.d(getClass().getName(), "new Camera: id = " + cameraId);
        mShutterCallback = null;
        mRawImageCallback = null;
        mJpegCallback = null;
        mPreviewCallback = null;
        mPostviewCallback = null;
        mZoomListener = null;

        Looper looper;
        if ((looper = Looper.myLooper()) != null) {
            mEventHandler = new EventHandler(this, looper);
        } else if ((looper = Looper.getMainLooper()) != null) {
            mEventHandler = new EventHandler(this, looper);
        } else {
            mEventHandler = null;
        }
        // Intent intent = new Intent(IRsmRegistry.class.getName());
        // Bind to the IRsmRegistry service
        // context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        cameraCaptureImageService = new CameraCaptureImageServiceImpl();
    }

    @Override
    protected void finalize() {
    }

    public final void release() {
        mcamera = null;
        // TODO Inform to the simulator that the real mobile don't want to use
        // the camera
    }

    public final void unlock() {
    }

    public final void lock() {
    }

    public final void reconnect() throws IOException {
    }

    public final void setPreviewDisplay(SurfaceHolder holder)
            throws IOException {
        mHolder = holder;
    }

    public interface PreviewCallback {

        void onPreviewFrame(byte[] data, Camera camera);
    };

    /**
     * The method starts a thread that simulate the camera showing images.
     */
    public final void startPreview() {
        cameraCaptureImageService.start(mcamera, mEventHandler, mHolder);
    }

    /**
     * The method stops the thread that simulate the camera showing images.
     */
    public final void stopPreview() {
        cameraCaptureImageService.stop();
    }

    public final boolean previewEnabled() {
        return true;
    }

    public final void setPreviewCallback(PreviewCallback cb) {
        mPreviewCallback = cb;
        mOneShot = false;
        mWithBuffer = false;
    }

    public final void setOneShotPreviewCallback(PreviewCallback cb) {
        mPreviewCallback = cb;
        mOneShot = true;
        mWithBuffer = false;
    }

    public final void setPreviewCallbackWithBuffer(PreviewCallback cb) {
        mPreviewCallback = cb;
        mOneShot = false;
        mWithBuffer = true;
    }

    public void addCallbackBuffer(byte[] callbackBuffer) {
    }

    private class EventHandler extends Handler {

        private Camera mCamera;

        public EventHandler(Camera c, Looper looper) {
            super(looper);
            mCamera = c;
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SET_IMAGE:
                    mCamera.setImage((CameraImageCapture) msg.obj);
                    return;
                case CAMERA_MSG_SHUTTER:
                    if (mShutterCallback != null) {
                        mShutterCallback.onShutter();
                    }
                    return;

                case CAMERA_MSG_RAW_IMAGE:
                    if (mRawImageCallback != null) {
                        mRawImageCallback.onPictureTaken((byte[]) msg.obj, mCamera);
                    }
                    return;

                case CAMERA_MSG_COMPRESSED_IMAGE:
                    if (mJpegCallback != null) {
                        mJpegCallback.onPictureTaken((byte[]) msg.obj, mCamera);
                    }
                    return;

                case CAMERA_MSG_PREVIEW_FRAME:
                    if (mPreviewCallback != null) {
                        PreviewCallback cb = mPreviewCallback;
                        if (mOneShot) {
                            // Clear the callback variable before the callback
                            // in case the app calls setPreviewCallback from
                            // the callback function
                            mPreviewCallback = null;
                        } else if (!mWithBuffer) {
                            // We're faking the camera preview mode to prevent
                            // the app from being flooded with preview frames.
                            // Set to oneshot mode again.
                        }
                        cb.onPreviewFrame((byte[]) msg.obj, mCamera);
                    }
                    return;

                case CAMERA_MSG_POSTVIEW_FRAME:
                    if (mPostviewCallback != null) {
                        mPostviewCallback.onPictureTaken((byte[]) msg.obj, mCamera);
                    }
                    return;

                case CAMERA_MSG_FOCUS:
                    if (mAutoFocusCallback != null) {
                        mAutoFocusCallback.onAutoFocus(
                                msg.arg1 == 0 ? false : true, mCamera);
                    }
                    return;

                case CAMERA_MSG_ZOOM:
                    if (mZoomListener != null) {
                        mZoomListener
                                .onZoomChange(msg.arg1, msg.arg2 != 0, mCamera);
                    }
                    return;

                case CAMERA_MSG_ERROR:
                    Log.e(TAG, "Error " + msg.arg1);
                    if (mErrorCallback != null) {
                        mErrorCallback.onError(msg.arg1, mCamera);
                    }
                    return;

                default:
                    Log.e(TAG, "Unknown message type " + msg.what);
                    return;
            }
        }
    }

    private void setImage(CameraImageCapture cic) {
        if (mHolder != null) {
            cameraCaptureImageService.drawImage(cic);
        }
    }

    /*
     * private static void postEventFromNative(Object camera_ref, int what, int
     * arg1, int arg2, Object obj) { Camera c =
     * (Camera)((WeakReference)camera_ref).get(); if (c == null) return;
     * 
     * if (c.mEventHandler != null) { Message m =
     * c.mEventHandler.obtainMessage(what, arg1, arg2, obj);
     * c.mEventHandler.sendMessage(m); } }
     */
    public interface AutoFocusCallback {

        void onAutoFocus(boolean success, Camera camera);
    };

    public final void autoFocus(AutoFocusCallback cb) {
        mAutoFocusCallback = cb;
    }

    public final void cancelAutoFocus() {
        mAutoFocusCallback = null;
    }

    public interface ShutterCallback {

        void onShutter();
    }

    public interface PictureCallback {

        void onPictureTaken(byte[] data, Camera camera);
    };

    public final void takePicture(ShutterCallback shutter, PictureCallback raw,
            PictureCallback jpeg) {
    }

    public final void takePicture(ShutterCallback shutter, PictureCallback raw,
            PictureCallback postview, PictureCallback jpeg) {
        mShutterCallback = shutter;
        mRawImageCallback = raw;
        mPostviewCallback = postview;
        mJpegCallback = jpeg;
    }

    public final void startSmoothZoom(int value) {
    }

    public final void stopSmoothZoom() {
    }

    public final void setDisplayOrientation(int degrees) {
    }

    public interface OnZoomChangeListener {

        void onZoomChange(int zoomValue, boolean stopped, Camera camera);
    };

    public final void setZoomChangeListener(OnZoomChangeListener listener) {
        mZoomListener = listener;
    }
    public static final int CAMERA_ERROR_UNKNOWN = 1;
    public static final int CAMERA_ERROR_SERVER_DIED = 100;

    public interface ErrorCallback {

        void onError(int error, Camera camera);
    };

    public final void setErrorCallback(ErrorCallback cb) {
        mErrorCallback = cb;
    }

    public void setParameters(Parameters params) {
    }

    public Parameters getParameters(android.hardware.Camera cam) {
    	// Camera.Parameters is an inner class and cannot exist without the superclass instance
    	
        Parameters p = null;
        try {
        Constructor<android.hardware.Camera.Parameters> constructor = 
        		(Constructor<Parameters>) android.hardware.Camera.Parameters.class.getDeclaredConstructors()[0];
        /*int k=0;
        for ( Constructor<?> cons:android.hardware.Camera.Parameters.class.getDeclaredConstructors()){
        	Log.e("leches","leches:"+ cons.getName()+":"+cons.getParameterTypes().length);
        	for (Class<?> type:cons.getParameterTypes()){
        		Log.e("leches1","leches1:"+ type.getName());
        	}
        	k++;
        	}*/
        constructor.setAccessible(true);
        
        p=(Parameters) constructor.newInstance(cam);
        Field keypreview = android.hardware.Camera.Parameters.class.getDeclaredField("KEY_PREVIEW_FORMAT");
        keypreview.setAccessible(true);
        String keypreviewvalue = keypreview.get(p).toString();
        Field pixelformat;
		
			pixelformat = android.hardware.Camera.Parameters.class.getDeclaredField("PIXEL_FORMAT_YUV420SP");
        pixelformat.setAccessible(true);
        String pixelformatvalue = pixelformat.get(p).toString();
        p.set(keypreviewvalue, pixelformatvalue);
        
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        
       // p.set(Parameters.KEY_PREVIEW_FORMAT, Parameters.PIXEL_FORMAT_YUV420SP);
        return p;
    }

    public class Size {

        public Size(int w, int h) {
            width = w;
            height = h;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Size)) {
                return false;
            }
            Size s = (Size) obj;
            return width == s.width && height == s.height;
        }

        @Override
        public int hashCode() {
            return width * 32713 + height;
        }
        public int width;
        public int height;
    };
}
