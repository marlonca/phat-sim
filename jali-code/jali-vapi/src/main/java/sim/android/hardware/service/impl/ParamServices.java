package sim.android.hardware.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class ParamServices {
	public static final String IP_SOCKET_UBIKSIM = "ubiksim_ip";

	public static Properties properties = null;
	
	public static void readProperties() throws IOException { 
		File myFile = new File("/sdcard/vapi.props");
		FileInputStream fIn = new FileInputStream(myFile);
		BufferedReader myReader = new BufferedReader(
				new InputStreamReader(fIn));
		
		properties = new Properties();
            properties.load(fIn);
		myReader.close();
	}
	
	public static String getProperty(String propertyName) {
		if(properties == null) {
			try {
				readProperties();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		return properties.getProperty(propertyName);
	}
}
