package sim.android.hardware.service.impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import sim.android.hardware.Camera;
import sim.android.hardware.service.CameraCaptureImageService;
import sim.android.hardware.service.CameraImageCapture;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import phat.mobile.servicemanager.client.ServiceManagerRemote;
import phat.mobile.servicemanager.services.Service;

public class CameraCaptureImageServiceImpl implements
        CameraCaptureImageService, Runnable {

    protected Socket s;
    protected ObjectInputStream oin;
    //  private byte[] image;
    //private final List<Bitmap> bitmaps = new ArrayList<Bitmap>();
    // private List<CameraImageCapture> cameraImageCaptures;
    protected Camera camera;
    protected Handler eventHandler;
    protected SurfaceHolder holder;
    private CameraCaptureImageService ccis;
    private Thread thread;
    Service cameraService;
    static final int IMG_BUFFER = 20;

    final Object mutex = new Object();
    
    public CameraCaptureImageServiceImpl() {
        //  cameraImageCaptures = new ArrayList<CameraImageCapture>();
    }

    private void connect() {
        if (cameraService != null) {
            return;
        }
        Log.d(getClass().getName(), "ServiceManagerRemote().getInstance()");
        ServiceManagerRemote smr = ServiceManagerRemote.getInstance();
        Log.d(getClass().getName(), "ServiceManagerRemote: " + smr);
        cameraService = smr.getService(null, Service.CAMERA);
        Log.d(getClass().getName(), "Service: " + cameraService);
        if (cameraService != null) {
            try {
                if (s == null) {
                    Log.d(getClass().getName(), "New socket: " + cameraService.getIp() + ":" + cameraService.getPort());
                    s = new Socket(cameraService.getIp(), cameraService.getPort());
                    s.setReceiveBufferSize(800 * 480 * 4 * IMG_BUFFER);
                }
                if (oin == null) {
                    oin = new ObjectInputStream(s.getInputStream());
                }
                System.out.println("AndroidMobile: InputStream obtenido");
                
            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                s = null;
                System.out.println("AndroidMobile: No accesible: 192.168.0.195:6450");
                return;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                oin = null;
                System.out.println("AndroidMobile: Problemas obteniendo InputStream");
            }
        }
    }

    public void run() {
        //WriteYUV yuv = new WriteYUV(800, 480);
        //yuv.startWriting();
        //BitmapFactory.Options opts = new BitmapFactory.Options();
        //opts.inPreferredConfig = Config.ARGB_8888;
        do {
            if (s == null || oin == null) {
                connect();
            }
            /*
             * try { sleep(wait); } catch (InterruptedException e) { return; }
             */
            if (s != null && s.isConnected()
                    && oin != null) {
                try {
                    //long t1 = System.currentTimeMillis();
                    CameraImageCapture cci = (CameraImageCapture) oin.readObject();
                    //long t2 = System.currentTimeMillis();
                    //Log.d(getClass().getName(), "readObject time = " + (t2 - t1));
                    if (cci != null) {
                        //Bitmap bitmap = BitmapFactory.decodeByteArray(cci.getImage(), 0, cci.getImage().length);
                        //Bitmap bitmap = Bitmap.createBitmap(cci.getImage(), 480, 800, Config.ARGB_8888);

                        //Bitmap bitmap = BitmapFactory.decodeByteArray(
                        //        cci.getImage(), 0, cci.getImage().length/*, opts*/);
                        notifyNewImage(cci);
                        //Bitmap bitmap = Bitmap.createBitmap(cci.getWidth(), cci.getHeight(), Config.RGB_565);
                        //   System.out.println("... Creando Bitmap = "+bitmap);
                        /*if (bitmap != null) {
                         synchronized (bitmaps) {
                         // cameraImageCaptures.add(cci);                            	
                         bitmaps.add(bitmap);
                         }
                         notifyNewImage(cci);
                         }*/
                    }
                } catch (ClassNotFoundException e1) {
                    // TODO Auto-generated catch block
                    Log.d(getClass().getSimpleName(), "ClassNotFoundException",
                            e1.getException());

                    stop();
                    break;
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    Log.d(getClass().getSimpleName(), "IOException: "
                            + e1.getStackTrace());
                    stop();
                    break;
                }
            } else {
                break;
            }
        } while (true);
    }

    public void stop() {
        thread.interrupt();
        try {
            if (oin != null) {
                oin.close();
            }
            if (s != null) {
                s.close();
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    protected void notifyNewImage(CameraImageCapture cic) {
        Message m;
        m = eventHandler.obtainMessage(SET_IMAGE, 0, 0, cic);
        eventHandler.sendMessage(m);
        m = eventHandler.obtainMessage(CAMERA_MSG_PREVIEW_FRAME, 0, 0, cic.getImage());
        eventHandler.sendMessage(m);
        m = eventHandler.obtainMessage(CAMERA_MSG_POSTVIEW_FRAME, 0, 0, cic.getImage());
        eventHandler.sendMessage(m);
        m = eventHandler.obtainMessage(CAMERA_MSG_SHUTTER, 0, 0, cic.getImage());
        eventHandler.sendMessage(m);
        m = eventHandler.obtainMessage(CAMERA_MSG_RAW_IMAGE, 0, 0, cic.getImage());
        eventHandler.sendMessage(m);
        m = eventHandler.obtainMessage(CAMERA_MSG_COMPRESSED_IMAGE, 0, 0, cic.getImage());
        eventHandler.sendMessage(m);
    }

    public void start(Camera camera, Handler eventHandler, SurfaceHolder holder) {
        Log.d(getClass().getName(), "start()");
        this.camera = camera;
        this.eventHandler = eventHandler;
        this.holder = holder;
        thread = new Thread(this);
        thread.start();
    }

    public void drawImage(CameraImageCapture cic) {
        Canvas c = null;
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeByteArray(
                    cic.getImage(), 0, cic.getImage().length);
            c = holder.lockCanvas();

            if (c != null) {
                int w = cic.getWidth();//cic.getWidth();
                int h = cic.getHeight();//cic.getHeight();
                int centerX = holder.getSurfaceFrame().centerX();
                int centerY = holder.getSurfaceFrame().centerY();
                synchronized (mutex) {
                    c.drawBitmap(bitmap, centerX - w / 2, centerY - h / 2, null);
                }
                //c.drawBitmap(cic.getImage(), 0, w, centerX - w / 2, centerY - h / 2, w, h, false, null);
            }
        } finally {
            // do this in a finally so that if an exception is thrown
            // during the above, we don't leave the Surface in an
            // inconsistent state
            if (c != null) {
                holder.unlockCanvasAndPost(c);
                if (bitmap != null) {
                    bitmap.recycle();
                }
            }
        }
    }

    /*public void drawImage(CameraImageCapture cic) {
     Canvas c = null;
     // CameraImageCapture cic = null;
     Bitmap bitmap = null;
     try {
     synchronized (bitmaps) {
     if (bitmaps.size() <= 0) {
     return;
     }
     bitmap = bitmaps.get(0);
     bitmaps.remove(0);
     c = holder.lockCanvas();
     if (c != null) {
     int w = cic.getWidth();//cic.getWidth();
     int h = cic.getHeight();//cic.getHeight();
     int centerX = holder.getSurfaceFrame().centerX();
     int centerY = holder.getSurfaceFrame().centerY();
     c.drawBitmap(bitmap, centerX - w / 2, centerY - h / 2, null);
     //c.drawBitmap(cic.getImage(), 0, w, centerX - w / 2, centerY - h / 2, w, h, false, null);
     }
     }
     } finally {
     // do this in a finally so that if an exception is thrown
     // during the above, we don't leave the Surface in an
     // inconsistent state
     if (c != null) {
     holder.unlockCanvasAndPost(c);
     bitmap.recycle();
     }
     }
     }*/
}
