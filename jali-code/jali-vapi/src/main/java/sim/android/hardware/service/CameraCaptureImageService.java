package sim.android.hardware.service;

import sim.android.hardware.Camera;
import android.os.Handler;
import android.view.SurfaceHolder;

public interface CameraCaptureImageService {
	// These match the enums in frameworks/base/include/camera/Camera.h
	static final int CAMERA_MSG_ERROR = 0x001;
	static final int CAMERA_MSG_SHUTTER = 0x002;
	static final int CAMERA_MSG_FOCUS = 0x004;
	static final int CAMERA_MSG_ZOOM = 0x008;
	static final int CAMERA_MSG_PREVIEW_FRAME = 0x010;
	static final int CAMERA_MSG_VIDEO_FRAME = 0x020;
	static final int CAMERA_MSG_POSTVIEW_FRAME = 0x040;
	static final int CAMERA_MSG_RAW_IMAGE = 0x080;
	static final int CAMERA_MSG_COMPRESSED_IMAGE = 0x100;
	static final int CAMERA_MSG_ALL_MSGS = 0x1FF;
	static final int SET_IMAGE = 0xFFFF; // Extra
	
	public void start(Camera camera, Handler eventHandler, SurfaceHolder holder);
	
	public void stop();
	
	public void drawImage(CameraImageCapture cic);
}
