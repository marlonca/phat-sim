package sim.android;

import java.io.IOException;


import android.hardware.Camera;
import android.view.SurfaceHolder;

public class CameraWrapper {

	private android.hardware.Camera realCamera=null;
	private sim.android.hardware.Camera simCamera=null;
	private boolean inEmulator=false;

	public CameraWrapper(boolean inEmulator) {
		this.inEmulator=inEmulator;
		realCamera=Camera.open();
	}


	public CameraWrapper open() {
		if (!inEmulator){
			realCamera=Camera.open();
			return this;
		}else {
			simCamera=sim.android.hardware.Camera.open();
			return this;
		}

	}


	public void setPreviewDisplay(SurfaceHolder holder) throws IOException {
		if (inEmulator)
			simCamera.setPreviewDisplay(holder);
		else
			realCamera.setPreviewDisplay(holder);

	}


	public void release() {
		if (inEmulator)
			simCamera.release();
		else
			realCamera.release();

	}


	public void stopPreview() {
		if (inEmulator)
			simCamera.stopPreview();
		else
			realCamera.stopPreview();

	}


	public android.hardware.Camera.Parameters getParameters() {
		if (inEmulator)
			return simCamera.getParameters(realCamera);
		else
			return realCamera.getParameters();
	}


	public void setDisplayOrientation(int i) {
		if (inEmulator)
			simCamera.setDisplayOrientation(i);
		else
			realCamera.setDisplayOrientation(i);

	}


	public void startPreview() {
		if (inEmulator)
			simCamera.startPreview();
		else
			realCamera.startPreview();
	}

}
