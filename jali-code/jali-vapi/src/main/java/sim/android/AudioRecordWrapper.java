package sim.android;

import android.media.AudioFormat;

public class AudioRecordWrapper {
	
	


	android.media.AudioRecord realAudio=null;
	sim.android.media.AudioRecord simAudio=null;
	
	public AudioRecordWrapper(boolean inEmulator, int audioSource, int sampleRateInHz, int channelConfig,
			int audioFormat, int bufferSizeInBytes) {	
		 if (inEmulator){			 
			 simAudio=new  sim.android.media.AudioRecord(audioSource,  sampleRateInHz,  channelConfig,
						 audioFormat,  bufferSizeInBytes);
		 }
		 else 
			 realAudio= new android.media.AudioRecord( audioSource,  sampleRateInHz,  channelConfig,
						 audioFormat,  bufferSizeInBytes);
	}
	
	/**
	 * Returns the minimum buffer size required for the successful creation of
	 * an AudioRecord object. Note that this size doesn't guarantee a smooth
	 * recording under load, and higher values should be chosen according to the
	 * expected frequency at which the AudioRecord instance will be polled for
	 * new data.
	 * 
	 * @param sampleRateInHz
	 *            the sample rate expressed in Hertz.
	 * @param channelConfig
	 *            describes the configuration of the audio channels. See
	 *            {@link AudioFormat#CHANNEL_IN_MONO} and
	 *            {@link AudioFormat#CHANNEL_IN_STEREO}
	 * @param audioFormat
	 *            the format in which the audio data is represented. See
	 *            {@link AudioFormat#ENCODING_PCM_16BIT}.
	 * @return {@link #ERROR_BAD_VALUE} if the recording parameters are not
	 *         supported by the hardware, or an invalid parameter was passed, or
	 *         {@link #ERROR} if the implementation was unable to query the
	 *         hardware for its output properties, or the minimum buffer size
	 *         expressed in bytes.
	 */
	static public int getMinBufferSize(int sampleRateInHz, int channelConfig,
			int audioFormat) {
		if (sampleRateInHz == 44100
				&& channelConfig == AudioFormat.CHANNEL_CONFIGURATION_MONO
				&& audioFormat == AudioFormat.ENCODING_PCM_16BIT)
			return 4096;
		else
			return -1;
	}

	public void startRecording() {
		if (realAudio!=null)
			realAudio.startRecording();
		else
			simAudio.startRecording();
		
	}

	public int read(byte[] buffer, int i, int bufferSize) {
 
		if (realAudio!=null)
			return realAudio.read(buffer,i,bufferSize);
		else
			return simAudio.read(buffer,i,bufferSize);
	}

	public void stop() {
		if (realAudio!=null)
			 realAudio.stop();
		else
			 simAudio.stop();
		
	}

}
