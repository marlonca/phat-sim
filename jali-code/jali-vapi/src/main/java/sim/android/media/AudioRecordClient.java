package sim.android.media;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import android.util.Log;

import sim.android.media.service.AudioStreamDataPacket;

public class AudioRecordClient {

	private final static String TAG = "AudioRecord";

	private String ip;
	private int port;
	private Socket socket;
	private ObjectInputStream ois;
	private boolean requireAudio = false;

	public AudioRecordClient(String ip, int port) {
		Log.d(TAG, "new AudioRecordClient(" + ip + ":" + port + ")");
		this.ip = ip;
		this.port = port;
	}

	public synchronized boolean native_start() {
		System.out.println("native_startRecording()");
		requireAudio = true;
		try {
			socket = new Socket(ip, port);
			ois = new ObjectInputStream(socket.getInputStream());
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public synchronized void native_stop() {
		requireAudio = false;
		try {
			if (ois != null)
				ois.close();
			if (socket != null)
				socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private AudioStreamDataPacket getAudioStreamDataPacket() {
		//System.out.println("getAudioStreamDataPacket()");
		if (socket != null && socket.isConnected() && ois != null) {
			try {				
				AudioStreamDataPacket asdp = (AudioStreamDataPacket) ois
						.readObject();
				return asdp;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public synchronized int native_read_in_byte_array(byte[] audioData,
			int offsetInBytes, int sizeInBytes) {

		AudioStreamDataPacket asdp = getAudioStreamDataPacket();
		if (asdp != null) {
			int numTotalBytes = asdp.numTotalBytes();
			if (numTotalBytes < sizeInBytes) {
				System.arraycopy(asdp.getData(), 0, audioData, offsetInBytes,
						numTotalBytes);
				return numTotalBytes;
			} else {
				System.arraycopy(asdp.getData(), 0, audioData, offsetInBytes,
						sizeInBytes);
				return sizeInBytes;
			}
		}
		return 0;
	}
}
