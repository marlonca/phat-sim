package sim.android.media;

import java.lang.IllegalArgumentException;
import java.lang.IllegalStateException;
import java.nio.ByteBuffer;
import android.media.AudioFormat;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import phat.mobile.servicemanager.client.ServiceManagerRemote;
import phat.mobile.servicemanager.services.Service;

/**
 * The AudioRecord class manages the audio resources for Java applications to
 * record audio from the audio input hardware of the platform. This is achieved
 * by "pulling" (reading) the data from the AudioRecord object. The application
 * is responsible for polling the AudioRecord object in time using one of the
 * following three methods: {@link #read(byte[],int, int)},
 * {@link #read(short[], int, int)} or {@link #read(ByteBuffer, int)}. The
 * choice of which method to use will be based on the audio data storage format
 * that is the most convenient for the user of AudioRecord.
 * <p>
 * Upon creation, an AudioRecord object initializes its associated audio buffer
 * that it will fill with the new audio data. The size of this buffer, specified
 * during the construction, determines how long an AudioRecord can record before
 * "over-running" data that has not been read yet. Data should be read from the
 * audio hardware in chunks of sizes inferior to the total recording buffer
 * size.
 */
public  class AudioRecord {
	// ---------------------------------------------------------
	// Constants
	// --------------------
	/**
	 * indicates AudioRecord state is not successfully initialized.
	 */
	public static final int STATE_UNINITIALIZED = 0;
	/**
	 * indicates AudioRecord state is ready to be used
	 */
	public static final int STATE_INITIALIZED = 1;

	/**
	 * indicates AudioRecord recording state is not recording
	 */
	public static final int RECORDSTATE_STOPPED = 1; // matches
														// SL_RECORDSTATE_STOPPED
	/**
	 * indicates AudioRecord recording state is recording
	 */
	public static final int RECORDSTATE_RECORDING = 3;// matches
														// SL_RECORDSTATE_RECORDING

	// Error codes:
	// to keep in sync with
	// frameworks/base/core/jni/android_media_AudioRecord.cpp
	/**
	 * Denotes a successful operation.
	 */
	public static final int SUCCESS = 0;
	/**
	 * Denotes a generic operation failure.
	 */
	public static final int ERROR = -1;
	/**
	 * Denotes a failure due to the use of an invalid value.
	 */
	public static final int ERROR_BAD_VALUE = -2;
	/**
	 * Denotes a failure due to the improper use of a method.
	 */
	public static final int ERROR_INVALID_OPERATION = -3;

	// Events:
	// to keep in sync with frameworks/base/include/media/AudioRecord.h
	/**
	 * Event id denotes when record head has reached a previously set marker.
	 */
	private static final int NATIVE_EVENT_MARKER = 2;
	/**
	 * Event id denotes when previously set update period has elapsed during
	 * recording.
	 */
	private static final int NATIVE_EVENT_NEW_POS = 3;

	private final static String TAG = "AudioRecord";

	// ---------------------------------------------------------
	// Member variables
	// --------------------
	/**
	 * The audio data sampling rate in Hz.
	 */
	private int mSampleRate = 22050;
	/**
	 * The number of input audio channels (1 is mono, 2 is stereo)
	 */
	private int mChannelCount = 1;
	/**
	 * The audio channel mask
	 */
	private int mChannels = AudioFormat.CHANNEL_IN_MONO;
	/**
	 * The current audio channel configuration
	 */
	private int mChannelConfiguration = AudioFormat.CHANNEL_IN_MONO;
	/**
	 * The encoding of the audio samples.
	 * 
	 * @see AudioFormat#ENCODING_PCM_8BIT
	 * @see AudioFormat#ENCODING_PCM_16BIT
	 */
	private int mAudioFormat = AudioFormat.ENCODING_PCM_16BIT;
	/**
	 * Where the audio data is recorded from.
	 */
	private int mRecordSource = MediaRecorder.AudioSource.DEFAULT;
	/**
	 * Indicates the state of the AudioRecord instance.
	 */
	private int mState = STATE_UNINITIALIZED;
	/**
	 * Indicates the recording state of the AudioRecord instance.
	 */
	private int mRecordingState = RECORDSTATE_STOPPED;
	/**
	 * Lock to make sure mRecordingState updates are reflecting the actual state
	 * of the object.
	 */
	private Object mRecordingStateLock = new Object();
	/**
	 * The listener the AudioRecord notifies when the record position reaches a
	 * marker or for periodic updates during the progression of the record head.
	 * 
	 * @see #setRecordPositionUpdateListener(OnRecordPositionUpdateListener)
	 * @see #setRecordPositionUpdateListener(OnRecordPositionUpdateListener,
	 *      Handler)
	 */
	private OnRecordPositionUpdateListener mPositionListener = null;
	/**
	 * Lock to protect position listener updates against event notifications
	 */
	private final Object mPositionListenerLock = new Object();

	private static AudioRecordClient audioRecordClient = null;

	// ---------------------------------------------------------
	// Constructor, Finalize
	// --------------------
	/**
	 * Class constructor.
	 * 
	 * @param audioSource
	 *            the recording source. See {@link MediaRecorder.AudioSource}
	 *            for recording source definitions.
	 * @param sampleRateInHz
	 *            the sample rate expressed in Hertz. Examples of rates are (but
	 *            not limited to) 44100, 22050 and 11025.
	 * @param channelConfig
	 *            describes the configuration of the audio channels. See
	 *            {@link AudioFormat#CHANNEL_IN_MONO} and
	 *            {@link AudioFormat#CHANNEL_IN_STEREO}
	 * @param audioFormat
	 *            the format in which the audio data is represented. See
	 *            {@link AudioFormat#ENCODING_PCM_16BIT} and
	 *            {@link AudioFormat#ENCODING_PCM_8BIT}
	 * @param bufferSizeInBytes
	 *            the total size (in bytes) of the buffer where audio data is
	 *            written to during the recording. New audio data can be read
	 *            from this buffer in smaller chunks than this size. See
	 *            {@link #getMinBufferSize(int, int, int)} to determine the
	 *            minimum required buffer size for the successful creation of an
	 *            AudioRecord instance. Using values smaller than
	 *            getMinBufferSize() will result in an initialization failure.
	 * @throws java.lang.IllegalArgumentException
	 */
	public AudioRecord(int audioSource, int sampleRateInHz, int channelConfig,
			int audioFormat, int bufferSizeInBytes)
			throws IllegalArgumentException {
		mState = STATE_UNINITIALIZED;
		mRecordingState = RECORDSTATE_STOPPED;

		Log.d(TAG, "new AudioRecord()");
		if (audioRecordClient == null) {
			Log.d(TAG, "ServiceManagerRemote().getInstance()");
			ServiceManagerRemote smr = ServiceManagerRemote.getInstance();
			Log.d(TAG, "ServiceManagerRemote: "+smr);
			Service service = smr.getService(null, Service.MICROPHONE);
			Log.d(TAG, "Service: "+service);
			if(service != null) {
				Log.d(TAG, "service: "+service.toString());
				audioRecordClient = new AudioRecordClient(service.getIp(),service.getPort());
				Log.d(TAG, "audioRecordClient: "+service.getIp()+":"+service.getPort());
			}
		}
		mState = STATE_INITIALIZED;
	}

	/**
	 * Releases the native AudioRecord resources. The object can no longer be
	 * used and the reference should be set to null after a call to release()
	 */
	public void release() {
		try {
			stop();
		} catch (IllegalStateException ise) {
			// don't raise an exception, we're releasing the resources.
		}
		// TODO algo mas?
		mState = STATE_UNINITIALIZED;
	}

	// --------------------------------------------------------------------------
	// Getters
	// --------------------
	/**
	 * Returns the configured audio data sample rate in Hz
	 */
	public int getSampleRate() {
		return mSampleRate;
	}

	/**
	 * Returns the audio recording source.
	 * 
	 * @see MediaRecorder.AudioSource
	 */
	public int getAudioSource() {
		return mRecordSource;
	}

	/**
	 * Returns the configured audio data format. See
	 * {@link AudioFormat#ENCODING_PCM_16BIT} and
	 * {@link AudioFormat#ENCODING_PCM_8BIT}.
	 */
	public int getAudioFormat() {
		return mAudioFormat;
	}

	/**
	 * Returns the configured channel configuration. See
	 * {@link AudioFormat#CHANNEL_IN_MONO} and
	 * {@link AudioFormat#CHANNEL_IN_STEREO}.
	 */
	public int getChannelConfiguration() {
		return mChannelConfiguration;
	}

	/**
	 * Returns the configured number of channels.
	 */
	public int getChannelCount() {
		return mChannelCount;
	}

	/**
	 * Returns the state of the AudioRecord instance. This is useful after the
	 * AudioRecord instance has been created to check if it was initialized
	 * properly. This ensures that the appropriate hardware resources have been
	 * acquired.
	 * 
	 * @see AudioRecord#STATE_INITIALIZED
	 * @see AudioRecord#STATE_UNINITIALIZED
	 */
	public int getState() {
		return mState;
	}

	/**
	 * Returns the recording state of the AudioRecord instance.
	 * 
	 * @see AudioRecord#RECORDSTATE_STOPPED
	 * @see AudioRecord#RECORDSTATE_RECORDING
	 */
	public int getRecordingState() {
		return mRecordingState;
	}

	/**
	 * Returns the minimum buffer size required for the successful creation of
	 * an AudioRecord object. Note that this size doesn't guarantee a smooth
	 * recording under load, and higher values should be chosen according to the
	 * expected frequency at which the AudioRecord instance will be polled for
	 * new data.
	 * 
	 * @param sampleRateInHz
	 *            the sample rate expressed in Hertz.
	 * @param channelConfig
	 *            describes the configuration of the audio channels. See
	 *            {@link AudioFormat#CHANNEL_IN_MONO} and
	 *            {@link AudioFormat#CHANNEL_IN_STEREO}
	 * @param audioFormat
	 *            the format in which the audio data is represented. See
	 *            {@link AudioFormat#ENCODING_PCM_16BIT}.
	 * @return {@link #ERROR_BAD_VALUE} if the recording parameters are not
	 *         supported by the hardware, or an invalid parameter was passed, or
	 *         {@link #ERROR} if the implementation was unable to query the
	 *         hardware for its output properties, or the minimum buffer size
	 *         expressed in bytes.
	 */
	static public int getMinBufferSize(int sampleRateInHz, int channelConfig,
			int audioFormat) {
		if (sampleRateInHz == 44100
				&& channelConfig == AudioFormat.CHANNEL_CONFIGURATION_MONO
				&& audioFormat == AudioFormat.ENCODING_PCM_16BIT)
			return 4096;
		else
			return -1;
	}

	// ---------------------------------------------------------
	// Transport control methods
	// --------------------
	/**
	 * Starts recording from the AudioRecord instance.
	 * 
	 * @throws IllegalStateException
	 */
	public void startRecording() throws IllegalStateException {
		if (mState != STATE_INITIALIZED) {
			throw (new IllegalStateException("startRecording() called on an "
					+ "uninitialized AudioRecord."));
		}

		// start recording
		synchronized (mRecordingStateLock) {
			if (audioRecordClient.native_start()) {
				mRecordingState = RECORDSTATE_RECORDING;
			}
		}
	}

	/**
	 * Stops recording.
	 * 
	 * @throws IllegalStateException
	 */
	public void stop() throws IllegalStateException {
		if (mState != STATE_INITIALIZED) {
			throw (new IllegalStateException(
					"stop() called on an uninitialized AudioRecord."));
		}

		// stop recording
		synchronized (mRecordingStateLock) {
			audioRecordClient.native_stop();
			mRecordingState = RECORDSTATE_STOPPED;
		}
	}

	// ---------------------------------------------------------
	// Audio data supply
	// --------------------
	/**
	 * Reads audio data from the audio hardware for recording into a buffer.
	 * 
	 * @param audioData
	 *            the array to which the recorded audio data is written.
	 * @param offsetInBytes
	 *            index in audioData from which the data is written expressed in
	 *            bytes.
	 * @param sizeInBytes
	 *            the number of requested bytes.
	 * @return the number of bytes that were read or or
	 *         {@link #ERROR_INVALID_OPERATION} if the object wasn't properly
	 *         initialized, or {@link #ERROR_BAD_VALUE} if the parameters don't
	 *         resolve to valid data and indexes. The number of bytes will not
	 *         exceed sizeInBytes.
	 */
	public int read(byte[] audioData, int offsetInBytes, int sizeInBytes) {
		if (mState != STATE_INITIALIZED) {
			return ERROR_INVALID_OPERATION;
		}

		if ((audioData == null) || (offsetInBytes < 0) || (sizeInBytes < 0)
				|| (offsetInBytes + sizeInBytes > audioData.length)) {
			return ERROR_BAD_VALUE;
		}

		
		return audioRecordClient.native_read_in_byte_array(audioData,
				offsetInBytes, sizeInBytes);
	}

	/**
	 * Reads audio data from the audio hardware for recording into a buffer.
	 * 
	 * @param audioData
	 *            the array to which the recorded audio data is written.
	 * @param offsetInShorts
	 *            index in audioData from which the data is written expressed in
	 *            shorts.
	 * @param sizeInShorts
	 *            the number of requested shorts.
	 * @return the number of shorts that were read or or
	 *         {@link #ERROR_INVALID_OPERATION} if the object wasn't properly
	 *         initialized, or {@link #ERROR_BAD_VALUE} if the parameters don't
	 *         resolve to valid data and indexes. The number of shorts will not
	 *         exceed sizeInShorts.
	 */
	public int read(short[] audioData, int offsetInShorts, int sizeInShorts) {
		/*
		 * if (mState != STATE_INITIALIZED) { return ERROR_INVALID_OPERATION; }
		 * 
		 * if ( (audioData == null) || (offsetInShorts < 0 ) || (sizeInShorts <
		 * 0) || (offsetInShorts + sizeInShorts > audioData.length)) { return
		 * ERROR_BAD_VALUE; }
		 * 
		 * return native_read_in_short_array(audioData, offsetInShorts,
		 * sizeInShorts);
		 */
		return 0;
	}

	/**
	 * Reads audio data from the audio hardware for recording into a direct
	 * buffer. If this buffer is not a direct buffer, this method will always
	 * return 0.
	 * 
	 * @param audioBuffer
	 *            the direct buffer to which the recorded audio data is written.
	 * @param sizeInBytes
	 *            the number of requested bytes.
	 * @return the number of bytes that were read or or
	 *         {@link #ERROR_INVALID_OPERATION} if the object wasn't properly
	 *         initialized, or {@link #ERROR_BAD_VALUE} if the parameters don't
	 *         resolve to valid data and indexes. The number of bytes will not
	 *         exceed sizeInBytes.
	 */
	public int read(ByteBuffer audioBuffer, int sizeInBytes) {
		/*
		 * if (mState != STATE_INITIALIZED) { return ERROR_INVALID_OPERATION; }
		 * 
		 * if ( (audioBuffer == null) || (sizeInBytes < 0) ) { return
		 * ERROR_BAD_VALUE; }
		 * 
		 * return native_read_in_direct_buffer(audioBuffer, sizeInBytes);
		 */
		return 0;
	}

	// ---------------------------------------------------------
	// Interface definitions
	// --------------------
	/**
	 * Interface definition for a callback to be invoked when an AudioRecord has
	 * reached a notification marker set by
	 * {@link AudioRecord#setNotificationMarkerPosition(int)} or for periodic
	 * updates on the progress of the record head, as set by
	 * {@link AudioRecord#setPositionNotificationPeriod(int)}.
	 */
	public interface OnRecordPositionUpdateListener {
		/**
		 * Called on the listener to notify it that the previously set marker
		 * has been reached by the recording head.
		 */
		void onMarkerReached(AudioRecord recorder);

		/**
		 * Called on the listener to periodically notify it that the record head
		 * has reached a multiple of the notification period.
		 */
		void onPeriodicNotification(AudioRecord recorder);
	}

	// ---------------------------------------------------------
	// Inner classes
	// --------------------

	/**
	 * Helper class to handle the forwarding of native events to the appropriate
	 * listener (potentially) handled in a different thread
	 */
	private class NativeEventHandler extends Handler {

		private final AudioRecord mAudioRecord;

		NativeEventHandler(AudioRecord recorder, Looper looper) {
			super(looper);
			mAudioRecord = recorder;
		}

		@Override
		public void handleMessage(Message msg) {
			OnRecordPositionUpdateListener listener = null;
			synchronized (mPositionListenerLock) {
				listener = mAudioRecord.mPositionListener;
			}

			switch (msg.what) {
			case NATIVE_EVENT_MARKER:
				if (listener != null) {
					listener.onMarkerReached(mAudioRecord);
				}
				break;
			case NATIVE_EVENT_NEW_POS:
				if (listener != null) {
					listener.onPeriodicNotification(mAudioRecord);
				}
				break;
			default:
				Log.e(TAG, "[ android.media.AudioRecord.NativeEventHandler ] "
						+ "Unknown event type: " + msg.what);
				break;
			}
		}
	};

	// ---------------------------------------------------------
	// Utility methods
	// ------------------

	private static void logd(String msg) {
		Log.d(TAG, "[ android.media.AudioRecord ] " + msg);
	}

	private static void loge(String msg) {
		Log.e(TAG, "[ android.media.AudioRecord ] " + msg);
	}

}
