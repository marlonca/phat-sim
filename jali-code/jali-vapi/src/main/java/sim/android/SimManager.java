package sim.android;

import android.app.Activity;
import android.os.Build;
import android.util.Log;

public class SimManager {

	public static CameraWrapper getCamera(){
		// by a-abiri and Ted Hopp
    	// from http://stackoverflow.com/questions/6864251/how-to-find-out-from-code-if-my-android-app-runs-on-emulator-or-real-device
		//original was "generic".equals(Build.BRAND.toLowerCase());
		 boolean inEmulator = Build.BRAND.toLowerCase().startsWith("generic");
		
		 return new CameraWrapper(inEmulator);
		 
	}
	

	public static AudioRecordWrapper getAudio(int audioSource, int sampleRateInHz, int channelConfig,
			int audioFormat, int bufferSizeInBytes){
		// by a-abiri and Ted Hopp
    	// from http://stackoverflow.com/questions/6864251/how-to-find-out-from-code-if-my-android-app-runs-on-emulator-or-real-device
		//original was "generic".equals(Build.BRAND.toLowerCase());
		 boolean inEmulator = Build.BRAND.toLowerCase().startsWith("generic");
		 return new AudioRecordWrapper(inEmulator, audioSource,  sampleRateInHz,  channelConfig,
					 audioFormat,  bufferSizeInBytes);
		
	}
	
	public static SensorManagerWrapper getSensorManager(Activity act){
		// by a-abiri and Ted Hopp
    	// from http://stackoverflow.com/questions/6864251/how-to-find-out-from-code-if-my-android-app-runs-on-emulator-or-real-device
		//original was "generic".equals(Build.BRAND.toLowerCase());
		 boolean inEmulator = Build.BRAND.toLowerCase().startsWith("generic");
		 return new SensorManagerWrapper(inEmulator, act);
		 
	}

	
	
	
}
