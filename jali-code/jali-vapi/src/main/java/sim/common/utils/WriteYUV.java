package sim.common.utils;

import java.io.*;

import android.graphics.Bitmap;
import android.graphics.Color;

public class WriteYUV {
	OutputStream dos;

	int width;
	int height;
	String type = "4:2:0";
	int oneFrameLength;
	byte oneFrame[];

	public WriteYUV(int width, int height) {
		this.width = width;
		this.height = height;
		this.type = "4:2:0"; // this version only support the 4:2:0 yuv
	}

	public void startWriting(OutputStream os) {
		try {
			dos = os;

			double pengali = 1.5;

			if (type.equals("4:2:0")) {
				pengali = 1.5;
			}

			oneFrameLength = (int) (width * height * (pengali));

			oneFrame = new byte[oneFrameLength];
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void startWriting() {
			double pengali = 1.5;

			if (type.equals("4:2:0")) {
				pengali = 1.5;
			}

			oneFrameLength = (int) (width * height * (pengali));

			oneFrame = new byte[oneFrameLength];
	}

	public void startWriting(String filename) {
		try {
			dos = new DataOutputStream(new BufferedOutputStream(
					new FileOutputStream(filename)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		startWriting(dos);
	}
	
	public byte[] getImage(Bitmap bImage) throws IOException {
		int i = 0;
		int numpixels = width * height;
		int R, G, B, Y, U, V;
		int ui = numpixels;
		int vi = numpixels + numpixels / 4;
		int s = 0;
		int color;

		for (int j = 0; j < height; j++)
			for (int k = 0; k < width; k++) {
				color = bImage.getPixel(k, j);
				R = Color.red(color);
				G = Color.green(color);
				B = Color.blue(color);

				Y = (int) ((0.257 * R) + (0.504 * G) + (0.098 * B) + 16);
				oneFrame[i] = (byte) Y;
				if (0 == j % 2 && 0 == k % 2) {
					oneFrame[vi++] = (byte) ((0.439 * R) - (0.368 * G) - (0.071 * B) + 128);
					oneFrame[ui++] = (byte) (-(0.148 *R) - (0.291 * G) + (0.439 * B) + 128);
				}
				i++;
			}
		return oneFrame;
	}
	
	/*
	public void writeImage(BufferedImage bi) throws IOException {
		int i = 0;
		int numpixels = width * height;
		int R, G, B, Y, U, V;
		int ui = numpixels;
		int vi = numpixels + numpixels / 4;
		int s = 0;
		int color;

		for (int j = 0; j < height; j++)
			for (int k = 0; k < width; k++) {
				color = bi.getRGB(k, j);
				R = color >> 16 & 0xff;
				G = color >> 8 & 0xff;
				B = color & 0xff;

				Y = (int) ((0.257 * R) + (0.504 * G) + (0.098 * B) + 16);
				oneFrame[i] = (byte) Y;
				if (0 == j % 2 && 0 == k % 2) {
					oneFrame[vi++] = (byte) ((0.439 * R) - (0.368 * G) - (0.071 * B) + 128);
					oneFrame[ui++] = (byte) (-(0.148 *R) - (0.291 * G) + (0.439 * B) + 128);
				}
				i++;
			}
		dos.write(oneFrame);
	}

	public void writeImage(BufferedImage bi, String a) throws IOException {
		int w = bi.getWidth();
		int h = bi.getHeight();

		boolean s = false;

		int arraySize = height * width;
		int op1 = width / 4;
		int op2 = arraySize + arraySize / 4;
		int R, G, B, Y, V;
		int U;
		int color;
		int yLoc;
		int uLoc;
		int vLoc;
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				color = bi.getRGB(i, j);

				// int alpha = color >> 24 & 0xff;
				R = color >> 16 & 0xff;
				G = color >> 8 & 0xff;
				B = color & 0xff;

				// Y = (int) ((0.257 * R) + (0.504 * G) + (0.098 * B) + 16);
				// V = (int) ((0.439 * R) - (0.368 * G) - (0.071 * B) + 128);
				// U = (int) (-(0.148 *R) - (0.291 * G) + (0.439 * B) + 128);

				Y = (int) (R * .299000 + G * .587000 + B * 0.114000);
				U = (int) (R * -.168736 + G * -.331264 + B * 0.500000 + 128);
				V = (int) (R * .500000 + G * -.418688 + B * -0.081312 + 128);

				// Y = (int)(0.299 * R + 0.587 * G + 0.114 * B);
				// U = (int)((B - Y) * 0.492f); //V = (int)((R - Y) * 0.877f);

				yLoc = j * width + i;
				uLoc = (j / 2) * (width / 2) + i / 2 + arraySize;
				vLoc = (j / 2) * (width / 2) + i / 2 + arraySize + arraySize / 4;
				// yLoc = j * width + i;
				// uLoc = j * op1 + i/2 + arraySize;
				// vLoc = j * op1 + i/2 + op2;

				oneFrame[yLoc] = (byte) Y;
				oneFrame[uLoc] = (byte) U;
				oneFrame[vLoc] = (byte) V;

				s = !s;
			}
		}

		dos.write(oneFrame);
	}

	public void writeImageYOnly(BufferedImage bi) {
		int w = bi.getWidth();
		int h = bi.getHeight();

		LinkedList<Byte> uBuffer = new LinkedList<Byte>();
		LinkedList<Byte> vBuffer = new LinkedList<Byte>();
		try {
			boolean s = false;

			for (int j = 0; j < h; j++) {
				for (int i = 0; i < w; i++) {
					int color = bi.getRGB(i, j);

					int alpha = color >> 24 & 0xff;
					int R = color >> 16 & 0xff;
					int G = color >> 8 & 0xff;
					int B = color & 0xff;

					// ~ int y = (int) ((0.257 * red) + (0.504 * green) + (0.098
					// * blue) + 16);
					// ~ int v = (int) ((0.439 * red) - (0.368 * green) - (0.071
					// * blue) + 128);
					// ~ int u = (int) (-(0.148 * red) - (0.291 * green) +
					// (0.439 * blue) + 128);

					int Y = (int) (R * .299000 + G * .587000 + B * 0.114000);
					int U = (int) (R * -.168736 + G * -.331264 + B * 0.500000 + 128);
					int V = (int) (R * .500000 + G * -.418688 + B * -0.081312 + 128);

					int arraySize = height * width;
					int yLoc = j * width + i;
					int uLoc = (j / 2) * (width / 2) + i / 2 + arraySize;
					int vLoc = (j / 2) * (width / 2) + i / 2 + arraySize
							+ arraySize / 4;

					oneFrame[yLoc] = (byte) Y;
					oneFrame[uLoc] = (byte) 128;
					oneFrame[vLoc] = (byte) 128;

					s = !s;
				}
			}

			for (int i = 0; i < oneFrameLength; i++) {
				dos.write(oneFrame[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public void endWriting() {
		try {
			dos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getOneFrameLength() {
		return oneFrameLength;
	}

}