Prerequisites
--------
- JDK 1.6+ installed as required for Android development
- Android SDK (r21.1 or later, latest is best supported) installed, preferably with all platforms, see http://developer.android.com/sdk/index.html
- Maven 3.1.1+ installed, see http://maven.apache.org/download.html
-Set environment variable ANDROID_HOME to the path of your installed Android SDK and add $ANDROID_HOME/tools as well as $ANDROID_HOME/platform-tools to your $PATH. (or on Windows %ANDROID_HOME%\tools and %ANDROID_HOME%\platform-tools). 

Licenses
--------
This software is distributed under the terms of the GPLv3 license. 
[License link](http://www.gnu.org/copyleft/gpl.html)
