package phat.mobile.adm;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import phat.mobile.servicemanager.CommonVars;

public class AndroidCommandTools {

    private static final int _BOOTING_SECONDS_TIMEOUT = 120;

    public static void createAndPushConfigFile(AndroidVirtualDevice avd, String ip, int port) {
        File file = new File(CommonVars.VAPI_FILE);
        try {
            file.createNewFile();
            PrintWriter writer = new PrintWriter(file);
            writer.println(CommonVars.PROP_SMS_IP + "=" + ip);
            writer.println(CommonVars.PROP_SMS_PORT + "=" + port);
            writer.println(CommonVars.PROP_DEVICE_NAME + "=" + avd.getSimulatedName());
            writer.close();

            pushFile(avd.getSerialNumber(), file.getAbsolutePath(), "/sdcard/" + CommonVars.VAPI_FILE);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("__________________________________________________________________________________________________________________________________");
            e.printStackTrace();
        }
    }

    public static void pushFile(String avdSerialNumber, String localAbsolutePath, String remoteAbsolutePath) {
        try {
            String so = System.getProperty("os.name");

            // options http://developer.android.com/tools/help/emulator.html
            String command = "adb -s " + avdSerialNumber + " push " + localAbsolutePath + " " + remoteAbsolutePath;

            System.out.println(command);
            Process p = null;
            if (so.contains("Windows")) {
                p = Runtime.getRuntime().exec("cmd /c " + command);
            } else {
                p = Runtime.getRuntime().exec(command);
            }
            try {
                p.waitFor();
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        p.getInputStream()));
                String line = reader.readLine();
                while (line != null) {
                    System.out.println(line);
                    line = reader.readLine();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static List<String> getAVDSerialNumber() {
        List<String> result = new ArrayList<String>();
        try {
            String so = System.getProperty("os.name");

            // options http://developer.android.com/tools/help/emulator.html
            String command = "adb devices";

            Process p = null;
            if (so.contains("Windows")) {
                p = Runtime.getRuntime().exec("cmd /c " + command);
            } else {
                p = Runtime.getRuntime().exec(command);
            }

            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line = reader.readLine();
            while (line != null) {
                for (String word : line.split("[ |\\t]")) {
                    if (word.contains("emulator")) {
                        result.add(word);
                        break;
                    }
                }

                line = reader.readLine();
            }

        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        return result;
    }

    public static void launchAVD(String avdName, String avdSerialNumber) {
        launchAVD(avdName, avdSerialNumber, "-dpi-device 240 -scale 0.5 -no-snapshot");
    }
    
    public static void launchAVD(String avdName, String avdSerialNumber, String options) {

        if (getAVDSerialNumber().contains(avdSerialNumber)) {
            System.out.println("AVD " + avdSerialNumber + " has already launched!");
            return;
        }
        try {
            String port = avdSerialNumber.substring(avdSerialNumber.lastIndexOf("-")+1);
            String so = System.getProperty("os.name");

            // options http://developer.android.com/tools/help/emulator.html
            String command = "emulator -port "+port+" "+ options +" -avd " + avdName;
            System.out.println(command);

            Process p = null;
            if (so.contains("Windows")) {
                System.out.println("Launching " + avdSerialNumber + " on Windows!");
                p = Runtime.getRuntime().exec("cmd /c " + command);
            } else {
                System.out.println("Launching " + avdSerialNumber + " on Linux!");
                p = Runtime.getRuntime().exec(command + "   -qemu -m 512 -enable-kvm ");

            }
            InputStream istr = p.getInputStream();
            final BufferedReader br = new BufferedReader(new InputStreamReader(istr));
            final BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            new Thread(new Runnable() {
                public void run() {
                    String s = "";
                    try {
                        while ((s = br.readLine()) != null) {
                            System.out.println(s);
                        }
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }).start();

            new Thread(new Runnable() {
                public void run() {
                    String s = "";
                    try {
                        while ((s = bre.readLine()) != null) {
                            System.err.println(s);
                        }
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }).start();
            waitForDeviceBooting(avdSerialNumber);
            unlockScreenOfDevice(avdSerialNumber);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private static void unlockScreenOfDevice(String avdSerialNumber) {
        try {
            String so = System.getProperty("os.name");

            // options http://developer.android.com/tools/help/emulator.html
            String command = "adb -s " + avdSerialNumber + "  shell input keyevent 82";

            Process p = null;
            if (so.contains("Windows")) {
                System.out.println("Launching " + avdSerialNumber + " on Windows!");
                p = Runtime.getRuntime().exec("cmd /c " + command);
            } else {
                System.out.println("Launching " + avdSerialNumber + " on Linux!");
                p = Runtime.getRuntime().exec(command);

            }
            InputStream istr = p.getInputStream();
            final BufferedReader br = new BufferedReader(new InputStreamReader(istr));
            final BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            new Thread(new Runnable() {
                public void run() {
                    String s = "";
                    try {
                        while ((s = br.readLine()) != null) {
                            System.out.println(s);
                        }
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }).start();

            new Thread(new Runnable() {
                public void run() {
                    String s = "";
                    try {
                        while ((s = bre.readLine()) != null) {
                            System.err.println(s);
                        }
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }).start();
            p.waitFor();

        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private static void waitForDeviceBooting(String serial) {

        try {
            String so = System.getProperty("os.name");


            // based on a shell script by Daan
            // http://blog.daanraman.com/coding/automatically-detect-if-the-android-emulator-is-running/
            String command = "adb -s " + serial + " shell getprop init.svc.bootanim ";
            try {
                Process p = null;
                Date initDate = new Date();
                boolean finishedBooting = false;
                do {

                    if (so.contains("Windows")) {
                        p = Runtime.getRuntime().exec("cmd /c " + command);
                    } else {
                        p = Runtime.getRuntime().exec(command);
                    }



                    InputStream istr = p.getInputStream();
                    final BufferedReader br = new BufferedReader(new InputStreamReader(istr));
                    final BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));

                    final StringBuffer stdout = new StringBuffer();
                    final StringBuffer stderr = new StringBuffer();
                    Thread t2 = new Thread(new Runnable() {
                        public void run() {
                            String s = "";
                            try {
                                while ((s = br.readLine()) != null) {
                                    stdout.append(s + "\n");
                                }
                            } catch (IOException e) {
                            }
                        }
                    });
                    t2.start();

                    Thread t1 = new Thread(new Runnable() {
                        public void run() {
                            String s = "";
                            try {
                                while ((s = bre.readLine()) != null) {
                                    stderr.append(s + "\n");
                                }
                            } catch (IOException e) {
                            }
                        }
                    });
                    t1.start();

                    p.waitFor();
                    t2.join(); // wait to flush the output
                    t1.join();// wait to flush the output

                    Thread.currentThread().sleep(1000);// give time from adb invocation to adb invocation
                    finishedBooting = stderr.toString().equalsIgnoreCase("stopped\n") || stdout.toString().equalsIgnoreCase("stopped\n");
                } while (!finishedBooting
                        && ((new Date().getTime() - initDate.getTime()) / 1000 < _BOOTING_SECONDS_TIMEOUT));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (IOException e1) {
            e1.printStackTrace();
        }

    }
}
