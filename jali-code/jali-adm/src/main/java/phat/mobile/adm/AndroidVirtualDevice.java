package phat.mobile.adm;

import com.android.chimpchat.ChimpChat;
import com.android.chimpchat.core.IChimpDevice;
import com.android.chimpchat.core.IChimpImage;
import com.android.chimpchat.core.PhysicalButton;
import com.android.chimpchat.core.TouchPressType;
import com.android.chimpchat.hierarchyviewer.HierarchyViewer;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import phat.mobile.servicemanager.CommonVars;

public class AndroidVirtualDevice {

    private static ChimpChat mChimpchat;
    private IChimpDevice device;
    private String simulatedName;
    private String avdName;
    private String serialNumber;
    private float displayHeight;
    private float displayWidth;
    private static final String ADB = "/platform-tools/adb";
    private String emuOptions = "-dpi-device 240 -scale 0.5 -no-snapshot";

    private static ChimpChat getChimpChat() {
        if (mChimpchat == null) {
            // Get value of ANDROID_HOME env variable
            Map<String, String> env = System.getenv();
            String androidHomeLoc = env.get("ANDROID_HOME");
            System.out.println("ANDROID_HOME = " + androidHomeLoc);

            //mChimpchat = ChimpChat.getInstance();
            TreeMap<String, String> options = new TreeMap<String, String>();
            options.put("backend", "adb");
            options.put("adbLocation", androidHomeLoc + ADB);
            mChimpchat = ChimpChat.getInstance(options);
        }
        return mChimpchat;
    }

    public static void shutdown() {
        if (mChimpchat != null) {
            mChimpchat.shutdown();
        }
    }

    /**
     * Adaptador para comunicarse con un emulador o dispositivo Android
     *
     * @param avdName	Nombre del dispositivo virtual android que se le da al
     * crearlo
     * @param serialNumber	Nombre identificativo de la instancia del emulador.
     * Es de la forma
     * @param simulatedName Nombre simbólico de la simulación que se usa para
     * obtener los servicios del ServiceManager (serviceSetId).
     */
    public AndroidVirtualDevice(String avdName, String serialNumber, String simulatedName) {
        this.simulatedName = simulatedName;
        this.serialNumber = serialNumber;
        this.avdName = avdName;

        System.out.println("AndroidVirtualDevice(" + avdName + "," + serialNumber + "," + simulatedName + ")");
        AndroidCommandTools.launchAVD(avdName, serialNumber, emuOptions);
        System.out.println(serialNumber + ": AVD Launched!");

        System.out.print(serialNumber + ": Connect...");
        boolean connected = connect();
        if (connected) {
            String height = device.getProperty("display.height");
            System.out.println("display.heiht:" + height);
            if (height != null) {
                displayHeight = Float.parseFloat(height);
            }
            String width = device.getProperty("display.width");
            System.out.println("display.width:" + width);
            if (width != null) {
                displayWidth = Float.parseFloat(width);
            }
        }
        System.out.println(" Done! " + connected);
    }

    public void sendConfigFileForService(String ip, int port) {
        System.out.print(serialNumber + ": Sending " + CommonVars.VAPI_FILE + " file...");
        AndroidCommandTools.createAndPushConfigFile(this, ip, port);
        System.out.println("Done!");
    }

    public static void main(String[] args) {
        AndroidVirtualDevice smartphone1 = new AndroidVirtualDevice("Smartphone1", "emulator-5554", "Smartphone1");
        AndroidVirtualDevice smartphone2 = new AndroidVirtualDevice("Smartphone2", "emulator-5558", "Smartphone2");
        AndroidVirtualDevice smartphone3 = new AndroidVirtualDevice("Smartphone3", "emulator-5560", "Smartphone3");
        AndroidVirtualDevice smartwatch = new AndroidVirtualDevice("SmartWatch1", "emulator-5556", "SmartWatch1");

        showProperties(smartphone1);
        showProperties(smartphone2);
        showProperties(smartphone3);
        showProperties(smartwatch);

        smartwatch.disconnect();
        System.out.println("Disconnect smartwatch!");

        smartphone1.disconnect();
        System.out.println("Disconnect smartphone1!");

        smartphone2.disconnect();
        System.out.println("Disconnect smartphone2!");

        smartphone3.disconnect();
        System.out.println("Disconnect smartphone3!");

        System.exit(0);
    }

    public String getEmuOptions() {
        return emuOptions;
    }

    public void setEmuOptions(String emuOptions) {
        this.emuOptions = emuOptions;
    }

    private static void showProperties(AndroidVirtualDevice avd) {
        System.out.println("\n\n\n" + avd.getAvdName() + "::::::::::::::::::::::::::::::::::::::");
        System.out.println("width = " + avd.getDisplayWidth() + ", height = " + avd.getDisplayHeight());
        avd.listProperties();
    }

    /**
     * List all properties.
     */
    public void listProperties() {
        if (device == null) {
            throw new IllegalStateException("init() must be called first.");
        }
        String result = "";
        for (String prop : device.getPropertyList()) {
            result += prop + ": " + device.getProperty(prop) + "\n";
        }
        System.out.println(result);
    }

    public float getDisplayHeight() {
        return displayHeight;
    }

    public float getDisplayWidth() {
        return displayWidth;
    }

    public boolean connect() {
        System.out.println("connect()");
        mChimpchat = getChimpChat();
        System.out.println("mChimpchat = " + mChimpchat);
        if (mChimpchat == null) {
            return false;
        }
        for (int i = 0; i < 10; i++) {
            device = mChimpchat.waitForConnection(30000, serialNumber);
            if (device != null) {
                return true;
            }
        }
        return false;
    }

    public void install(String apkFile) {
        device.installPackage(apkFile);
    }

    public void disconnect() {
        device.dispose();
    }

    public void pressBackPhysicalButton() {
        device.press(PhysicalButton.BACK, TouchPressType.DOWN_AND_UP);
    }

    public String getFocusedWindowName() {
        HierarchyViewer hv = device.getHierarchyViewer();
        if (hv != null) {
            return hv.getFocusedWindowName();
        }
        return null;
    }

    public void pressHomePhysicalButton() {
        device.press(PhysicalButton.HOME, TouchPressType.DOWN_AND_UP);
    }

    public void pressMenuPhysicalButton() {
        device.press(PhysicalButton.MENU, TouchPressType.DOWN_AND_UP);
    }

    public void tap(int x, int y) {
        try {
            device.getManager().tap(x, y);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void touch(int x, int y) {
        try {
            device.getManager().touch(x, y);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void touchDown(int x, int y) {
        try {
            device.getManager().touchDown(x, y);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void touchUp(int x, int y) {
        try {
            device.getManager().touchUp(x, y);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void touchMove(int x, int y) {
        try {
            device.getManager().touchMove(x, y);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * Despierta al dispositivo
     */
    public void wake() {
        device.wake();
    }

    /**
     * Takes a snapshot of the virtual android device
     *
     * @return
     */
    public BufferedImage takeSnapshot() {
        IChimpImage image = device.takeSnapshot();
        return image.getBufferedImage();
    }

    /**
     * Desbloquea el dispositivo
     */
    public void unlock() {
        /**
         * Perform a drag from one one location to another
         *
         * @param startx the x coordinate of the drag's starting point
         * @param starty the y coordinate of the drag's starting point
         * @param endx the x coordinate of the drag's end point
         * @param endy the y coordinate of the drag's end point
         * @param steps the number of steps to take when interpolating points
         * @param ms the duration of the drag
         */
        device.drag(240, 650, 450, 650, 10, 500);
    }

    /**
     * Escribe el texto pasado como parámetro en el campo de texto previamente
     * señalado.
     *
     * @param string
     * @throws IOException
     */
    public void writeText(String string) throws IOException {
        char[] charSequence = string.toCharArray();
        for (char s : charSequence) {
            switch (s) {
                case ' ':
                    device.getManager().keyDown("KEYCODE_SPACE");
                    break;
                default:
                    device.type(String.valueOf(s));
                    break;
            }
        }
    }

    /**
     * Realiza el gesto de arrastrar el dedo de izquierda a derecha
     */
    public void dragLeftToRight() {
        /**
         * Perform a drag from one one location to another
         *
         * @param startx the x coordinate of the drag's starting point
         * @param starty the y coordinate of the drag's starting point
         * @param endx the x coordinate of the drag's end point
         * @param endy the y coordinate of the drag's end point
         * @param steps the number of steps to take when interpolating points
         * @param ms the duration of the drag
         */
        device.drag(150, 520, 450, 500, 10, 0);
    }

    /**
     * Realiza el gesto de arrastrar el dedo de derecha a izquierda
     */
    public void dragRightToLeft() {
        /**
         * Perform a drag from one one location to another
         *
         * @param startx the x coordinate of the drag's starting point
         * @param starty the y coordinate of the drag's starting point
         * @param endx the x coordinate of the drag's end point
         * @param endy the y coordinate of the drag's end point
         * @param steps the number of steps to take when interpolating points
         * @param ms the duration of the drag
         */
        device.drag(450, 520, 150, 500, 10, 0);
    }

    public void startActivity(String pkg, String activity) {
        String uri = null;
        String action = "android.intent.action.MAIN";
        String data = null;
        String mimeType = null;
        Collection categories = new ArrayList();
        Map extras = new HashMap();
        //String pkg = "es.um.diic.parkinson";
        //String activity = "ParkinsonActivity";
        String component = pkg + "/." + activity;
        int flags = 0;
        /**
         * Start an activity.
         *
         * @param uri the URI for the Intent
         * @param action the action for the Intent
         * @param data the data URI for the Intent
         * @param mimeType the mime type for the Intent
         * @param categories the category names for the Intent
         * @param extras the extras to add to the Intent
         * @param component the component of the Intent
         * @param flags the flags for the Intent
         *
         * void startActivity(
         * @Nullable String uri,
         * @Nullable String action,
         * @Nullable String data,
         * @Nullable String mimeType, Collection categories, Map extras,
         * @Nullable String component, int flags);
         */
        device.startActivity(uri, action, data, mimeType, categories, extras,
                component, flags);
    }

    public String getSimulatedName() {
        return simulatedName;
    }

    public String getAvdName() {
        return avdName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }
}
