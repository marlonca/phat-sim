package sim.android.hardware.service;

import java.io.Serializable;

public class SimSensorEvent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * A constant describing an accelerometer sensor type. See
     * {@link android.hardware.SensorEvent#values SensorEvent.values} for more
     * details.
     */
    public static final int TYPE_ACCELEROMETER = 1;

    /**
     * A constant describing a magnetic field sensor type. See
     * {@link android.hardware.SensorEvent#values SensorEvent.values} for more
     * details.
     */
    public static final int TYPE_MAGNETIC_FIELD = 2;

    /**
     * A constant describing an orientation sensor type. See
     * {@link android.hardware.SensorEvent#values SensorEvent.values} for more
     * details.
     *
     * @deprecated use {@link android.hardware.SensorManager#getOrientation
     *             SensorManager.getOrientation()} instead.
     */
    @Deprecated
    public static final int TYPE_ORIENTATION = 3;

    /** A constant describing a gyroscope sensor type */
    public static final int TYPE_GYROSCOPE = 4;

    /**
     * A constant describing an light sensor type. See
     * {@link android.hardware.SensorEvent#values SensorEvent.values} for more
     * details.
     */
    public static final int TYPE_LIGHT = 5;

    /** A constant describing a pressure sensor type */
    public static final int TYPE_PRESSURE = 6;

    /** A constant describing a temperature sensor type */
    public static final int TYPE_TEMPERATURE = 7;

    /**
     * A constant describing an proximity sensor type. See
     * {@link android.hardware.SensorEvent#values SensorEvent.values} for more
     * details.
     */
    public static final int TYPE_PROXIMITY = 8;

    /**
     * A constant describing a gravity sensor type.
     * See {@link android.hardware.SensorEvent SensorEvent}
     * for more details.
     */
    public static final int TYPE_GRAVITY = 9;

    /**
     * A constant describing a linear acceleration sensor type.
     * See {@link android.hardware.SensorEvent SensorEvent}
     * for more details.
     */
    public static final int TYPE_LINEAR_ACCELERATION = 10;

    /**
     * A constant describing a rotation vector sensor type.
     * See {@link android.hardware.SensorEvent SensorEvent}
     * for more details.
     */
    public static final int TYPE_ROTATION_VECTOR = 11;
    
    public static final int PORT = 34567;
    
    private float[] values;
    private int type;
    private int accuracy;
    private long step;
    
    public SimSensorEvent(int type, float[] values, long step, int accuracy) {
    	this.type = type;
    	this.values = values;
    	this.step = step;
    	this.accuracy = accuracy;
    }
    
	public float[] getValues() {
		return values;
	}
	public int getType() {
		return type;
	}
	public long getStep() {
		return step;
	}
	public int getAccuracy() {
		return accuracy;
	}
	
	@Override
	public String toString() {
		super.toString();
		String result = "1;step="+step+";Type=";
		switch(type) {
		case TYPE_ACCELEROMETER:
			result += "TYPE_ACCELEROMETER";
			break;
		case TYPE_MAGNETIC_FIELD:
			result += "TYPE_MAGNETIC_FIELD";
			break;
		case TYPE_ORIENTATION:
			result += "TYPE_ORIENTATION";
			break;
		}
		
		result += ";accuracy="+accuracy+";"+values[0]+";"+""+values[1]+";"+""+values[2]+"";
		
		return result;
	}
}
