package sim.android.hardware.service;

import java.io.Serializable;

/**
 * Captura de imagen del simulador que se env�a al m�vil
 *
 * @author pablo
 *
 */
public class CameraImageCapture implements Serializable {

    public static final int PORT = 6450;
    protected long step;
    protected byte[] image;
    protected int type;
    protected int width;
    protected int height;

    public CameraImageCapture(long step, byte[] image, int width, int heigth, int type) {
        this.step = step;
        this.image = image;
        this.width = width;
        this.height = heigth;
        this.type = type;
    }

    public long getStep() {
        return step;
    }

    public byte[] getImage() {
        return image;
    }

    public int getType() {
        return type;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "step=" + step + ",image=" + image + ",width=" + width + ",height=" + height + ",type=" + type;
    }
}
