package sim.android.media.service;

import java.io.Serializable;

public class AudioStreamDataPacket implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int PORT = 10000;
	public static final int REDIRECT_PORT = 10100;
	
	private int frequency = 44100;
	private int channelConfiguration = 2; //AudioFormat.CHANNEL_CONFIGURATION_MONO;
	private int audioEncoding = 2; //AudioFormat.ENCODING_PCM_16BIT;
	private byte[] data;
	private int numSamples;
	private int numReadings;
	
	public AudioStreamDataPacket(byte[] data, int numSamples, int numReadings) {
		super();
		this.data = data;
		this.numSamples = numSamples;
		this.numReadings = numReadings;
	}

	public int getFrequency() {
		return frequency;
	}

	public int getChannelConfiguration() {
		return channelConfiguration;
	}

	public int getAudioEncoding() {
		return audioEncoding;
	}

	public byte[] getData() {
		return data;
	}

	public int getNumSamples() {
		return numSamples;
	}

	public int getNumReadings() {
		return numReadings;
	}
	
	public int numTotalBytes() {
		return numSamples*numReadings;
	}
}
