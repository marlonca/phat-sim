package sim.android.app.state.message;

public class GeoQRLocationStateMessage extends ActivityStateMessage {
	private float x;
	private float y;
	private int floor;

	public GeoQRLocationStateMessage(String componentName) {
		super(componentName);
	}
	
	public GeoQRLocationStateMessage(String pkg, String activity) {
		super(pkg, activity);
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	
}
