package sim.android.app.state.message;

import java.io.Serializable;

public abstract class ActivityStateMessage implements Serializable {
	public static final int PORT = 6455;
	
	private String pkg;
	private String activity;
	
	public ActivityStateMessage(String componentName) {
		int index = componentName.lastIndexOf(".");
		pkg = componentName.substring(0, index);
		activity = componentName.substring(index+1);
	}
	
	public ActivityStateMessage(String pkg, String activity) {
		this.pkg = pkg;
		this.activity = activity;
	}

	public String getPkg() {
		return pkg;
	}

	public String getActivity() {
		return activity;
	}
	
	
}
