/**
 * 
 */
package phat.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * @author UCM
 *
 */
public class SimulationFileReadConfig {
	
	public static void main(String[] args) {

	JSONParser parser = new JSONParser();
	JSONArray temp;

		try {
			Object obj = parser.parse(new FileReader("/home/mcardenas/git/phatsim/simulation_example_v0.json"));
			JSONObject jsonObject = (JSONObject) obj;
			
			temp = new JSONArray();
			temp = (JSONArray) jsonObject.get("bodies");
			
			for(int j=0; j < temp.size();j++){
				JSONObject obj_ = (JSONObject) temp.get(j);
				System.out.println(obj_.get("name"));
			}
						
			temp = new JSONArray();
			temp = (JSONArray) jsonObject.get("commands");
			for(int i=0;i < temp.size();i++){
				JSONObject obj_ = (JSONObject) temp.get(i);
				System.out.println(obj_.get("name"));
			}
	
		} catch(FileNotFoundException ex1){
			ex1.printStackTrace();
		} catch(IOException ex2){
			ex2.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
	
	}
}