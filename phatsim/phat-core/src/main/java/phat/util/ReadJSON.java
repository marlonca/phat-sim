/**
 * 
 */
package phat.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import phat.beans.Body;
import phat.beans.Command;
import phat.beans.PhatSimulationBean;

/**
 * Class read configuration from file JSON.
 * @author mcardenas
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ReadJSON {
	
	/**
     * Initialing param for simulation 
     * @param url
     * @return
     */
    public static PhatSimulationBean initPhatSimConfig(String url) {
		
		JSONParser parser;
		JSONObject jsonObject;
		Gson gson = new Gson();
		
		PhatSimulationBean phatSimulation = null;
		
		try {
			parser = new JSONParser();
			Object obj_ = parser.parse(new FileReader(url));
			jsonObject = (JSONObject) obj_;
			phatSimulation = (gson.fromJson(jsonObject.toString(), PhatSimulationBean.class));
		} catch(FileNotFoundException ex1){
			ex1.printStackTrace();
		} catch(IOException ex2){
			ex2.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
		return phatSimulation;
	}
    
    /**
     * Initialing commands for execution in the simulation.
     * @param url
     * @return commands.
     */
    public static HashMap<String, Object> initPhatSimCommands(String url) {
    	JSONObject jsonObject;
    	JSONArray temp;
    	
		JSONParser parser = new JSONParser();
		Gson gson = new Gson();
		Object command;
		
		PhatSimulationBean phatSimulationBean = new PhatSimulationBean();
	    
		try {
			parser = new JSONParser();
			Object obj_ = parser.parse(new FileReader(url));
			jsonObject = (JSONObject) obj_;
			
			temp = new JSONArray();
			temp = (JSONArray) jsonObject.get("commands_");
			if(temp != null && temp.size() > 0){
				phatSimulationBean.setCommands(new HashMap<String,Object>());
				for(int i=0;i < temp.size();i++){
					command = new ArrayList<Object>();
					JSONObject obj = (JSONObject) temp.get(i);
					try {
						command = gson.fromJson(obj.toString(), Class.forName(obj.get("packageClass").toString() + 
								"." + obj.get("typeClass").toString()));
						phatSimulationBean.getCommands().put(obj.get("id").toString(), command);
					} catch (JsonSyntaxException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e){
						e.printStackTrace();
					}
				}
			}
		} catch(FileNotFoundException ex1){
			ex1.printStackTrace();
		} catch(IOException ex2){
			ex2.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
		return phatSimulationBean.getCommands();
	}
    
    
	public static boolean initJSONConfig(String url) {
		
		boolean ret = false;
		JSONParser parser = new JSONParser();
		JSONArray temp;
		JSONObject jsonObject;
		HashMap<String, Object> config;
		
		List<Object> bodies;
		List<Object> commands;
		
		Gson gson = new Gson();
	    
		try {
			parser = new JSONParser();
			Object obj_ = parser.parse(new FileReader(url));
			jsonObject = (JSONObject) obj_;
			
			config = new HashMap();
			config.put("name", jsonObject.get("name").toString());
			config.put("debug", (Boolean)jsonObject.get("debug"));
			config.put("worlstation", jsonObject.get("worlstation").toString());
			config.put("nameHouse", jsonObject.get("nameHouse").toString());
			config.put("typeHouse", jsonObject.get("typeHouse").toString());
			
			temp = new JSONArray();
			temp = (JSONArray) jsonObject.get("bodies");
			if(temp.size() > 0){
				bodies = new ArrayList();
				for(int j=0; j < temp.size();j++){
					JSONObject obj = (JSONObject) temp.get(j);
					Body body = gson.fromJson(obj.toString(), Body.class);
					bodies.add(body);
				}	
			}
			
			temp = new JSONArray();
			temp = (JSONArray) jsonObject.get("commands");
			if(temp.size() > 0){
				commands = new ArrayList();
				for(int i=0;i < temp.size();i++){
					JSONObject obj = (JSONObject) temp.get(i);
					Command command = gson.fromJson(obj.toString(), Command.class);
					commands.add(command);
				}
			}
			ret = true;
		} catch(FileNotFoundException ex1){
			ex1.printStackTrace();
		} catch(IOException ex2){
			ex2.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
