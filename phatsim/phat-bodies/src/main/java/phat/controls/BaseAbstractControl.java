/**
 * UCM SociAAL Package.
 */
package phat.controls;

import java.io.IOException;

import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;

import phat.body.control.parkinson.LeftArmMoveControl;

/**
 * 
 * BaseAbstractControl class.
 * @author ucm
 *
 */
public abstract class BaseAbstractControl extends AbstractControl {
	
	/**
	 * Axis x Eje x
	 */
	static final int AXIS_X = 0;
	
	/**
	 * Axis y Eje y
	 */
	static final int AXIS_Y = 1;
	
	/**
	 * Axis z Eje z
	 */
	static final int AXIS_Z = 2;
	
	/**
	 * Angulos para rotación/traslación
	 */
	private float[] angles = new float[3];
	
	
    /**
     * Indice de Angulo de incidencia. 
     */
    private int index;
    
    /**
     * Ángulo de apertura mínima.
     */
    private float minAngle;
    
    /**
     * Ángulo de apertura máxima.
     */
    private float maxAngle;
    
    /**
     * Velocidad angular aplicada en el movimiento.
     */
    private float angular;
    
    /* (non-Javadoc)
     * @see com.jme3.scene.control.AbstractControl#cloneForSpatial(com.jme3.scene.Spatial)
     */
    @Override
    public Control cloneForSpatial(Spatial sptl) {
        LeftArmMoveControl control = new LeftArmMoveControl();
        control.setSpatial(sptl);
        control.setAngular(angular);
        control.setMaxAngle(maxAngle);
        control.setMinAngle(minAngle);
        return control;
    }
    
    /* (non-Javadoc)
     * @see com.jme3.scene.control.AbstractControl#write(com.jme3.export.JmeExporter)
     */
    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule oc = ex.getCapsule(this);
        oc.write(angular, "angular", angular);
        oc.write(maxAngle, "maxAngle", maxAngle);
        oc.write(minAngle, "minAngle", minAngle);
        
    }

    /* (non-Javadoc)
     * @see com.jme3.scene.control.AbstractControl#read(com.jme3.export.JmeImporter)
     */
    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        InputCapsule ic = im.getCapsule(this);
        angular = ic.readFloat("angular", angular);
        maxAngle = ic.readFloat("maxAngle", maxAngle);
        minAngle = ic.readFloat("minAngle", minAngle);
    }
    
    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        
    }
    
    /**
	 * @return the angles
	 */
	public float[] getAngles() {
		return angles;
	}

	/**
	 * @param angles the angles to set
	 */
	public void setAngles(float[] angles) {
		this.angles = angles;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return the minAngle
	 */
	public float getMinAngle() {
		return minAngle;
	}

	/**
	 * @param minAngle the minAngle to set
	 */
	public void setMinAngle(float minAngle) {
		this.minAngle = minAngle;
	}

	/**
	 * @return the maxAngle
	 */
	public float getMaxAngle() {
		return maxAngle;
	}

	/**
	 * @param maxAngle the maxAngle to set
	 */
	public void setMaxAngle(float maxAngle) {
		this.maxAngle = maxAngle;
	}

	/**
	 * @return the angular
	 */
	public float getAngular() {
		return angular;
	}

	/**
	 * @param angular the angular to set
	 */
	public void setAngular(float angular) {
		this.angular = angular;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BaseAbstractControl [angles=" + "{" + angles[0] + "," + angles[1] + ","+ angles[2]+ "}" + ", index=" + index + ", minAngle=" + minAngle + ", maxAngle="
				+ maxAngle + ", angular=" + angular + "]";
	}
	
	

}
