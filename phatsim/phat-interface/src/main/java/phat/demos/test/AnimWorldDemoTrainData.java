
package phat.demos.test;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.logging.Logger;

import phat.app.PHATApplication;
import phat.app.PHATInitAppListener;
import phat.body.BodiesAppState;
import phat.body.commands.FallDownCommand;
import phat.body.commands.PlayBodyAnimationCommand;
import phat.body.commands.RandomWalkingCommand;
import phat.body.commands.SetBodyInCoordenatesCommand;
import phat.body.commands.SetCameraToBodyCommand;
import phat.body.commands.SetSpeedDisplacemenetCommand;
import phat.body.commands.StandUpCommand;
import phat.commands.PHATCommand;
import phat.devices.DevicesAppState;
import phat.devices.commands.CreateAccelerometerSensorCommand;
import phat.devices.commands.CreateSmartphoneCommand;
import phat.devices.commands.SetDeviceOnPartOfBodyCommand;
import phat.sensors.accelerometer.AccelerometerControl;
import phat.sensors.accelerometer.XYAccelerationsChart;
import phat.server.ServerAppState;
import phat.server.commands.ActivateAccelerometerServerCommand;
import phat.structures.houses.TestHouse;
import phat.util.Debug;
import phat.util.SpatialFactory;
import phat.world.WorldAppState;

/**
 *
 * @author pablo
 */

class ServerStatus {

	private String status = "notInitialised";
	private int port;
	private boolean finish = false;

	public ServerStatus(int port) {
		this.port = port;
	}

	public synchronized String getStatus() {
		return status;
	}

	public synchronized void setStatus(String status) {
		this.status = status;
	}

	public void serve() {
		new Thread() {
			public void run() {
				ServerSocket ss;
				try {
					ss = new ServerSocket(port);
					while (!finish) {
						final Socket connectedClient = ss.accept();
						new Thread() {
							public void run() {
								try {
									connectedClient.getOutputStream().write(getStatus().getBytes());
									connectedClient.close();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}.start();

					}
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}.start();
	}
}

public class AnimWorldDemoTrainData implements PHATInitAppListener {

	private static final Logger logger = Logger.getLogger(TestHouse.class.getName());
	BodiesAppState bodiesAppState;
	ServerAppState serverAppState;
	DevicesAppState devicesAppState;
	WorldAppState worldAppState;

	public static void main(String[] args) {
		AnimWorldDemoTrainData test = new AnimWorldDemoTrainData();
		PHATApplication phat = new PHATApplication(test);
		phat.setDisplayFps(true);
		phat.setDisplayStatView(false);
		AppSettings settings = new AppSettings(true);
		settings.setTitle("PHAT");
		settings.setWidth(640);
		settings.setHeight(480);
		phat.setSettings(settings);
		phat.start();

	}

	ServerStatus ss = new ServerStatus(60003);

	@Override
	public void init(SimpleApplication app) {
		SpatialFactory.init(app.getAssetManager(), app.getRootNode());

		ss.serve();

		AppStateManager stateManager = app.getStateManager();

		app.getFlyByCamera().setMoveSpeed(10f);

		app.getCamera().setLocation(new Vector3f(0.2599395f, 2.7232018f, 3.373138f));
		app.getCamera().setRotation(new Quaternion(-0.0035931943f, 0.9672268f, -0.25351822f, -0.013704466f));

		BulletAppState bulletAppState = new BulletAppState();
		bulletAppState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
		stateManager.attach(bulletAppState);
		bulletAppState.getPhysicsSpace().setAccuracy(1 / 60f);
		// bulletAppState.setDebugEnabled(true);

		worldAppState = new WorldAppState();
		worldAppState.setLandType(WorldAppState.LandType.Grass);
		app.getStateManager().attach(worldAppState);
		worldAppState.setCalendar(2013, 1, 1, 12, 0, 0);

		Debug.enableDebugGrid(10, app.getAssetManager(), app.getRootNode());
		bodiesAppState = new BodiesAppState();
		stateManager.attach(bodiesAppState);

		bodiesAppState.createBody(BodiesAppState.BodyType.ElderLP, "Patient");
		bodiesAppState.runCommand(new SetBodyInCoordenatesCommand("Patient", Vector3f.ZERO));

		//bodiesAppState.runCommand(new PlayBodyAnimationCommand("Patient","RunForward"));
		//bodiesAppState.runCommand(new RandomWalkingCommand("Patient", true));

		bodiesAppState.runCommand(new SetSpeedDisplacemenetCommand("Patient", 2.1f));

		SetCameraToBodyCommand camCommand = new SetCameraToBodyCommand("Patient");
		camCommand.setDistance(3);
		camCommand.setFront(true);
		bodiesAppState.runCommand(camCommand);

		devicesAppState = new DevicesAppState();
		stateManager.attach(devicesAppState);

		// Creation smartphone dispositive
		devicesAppState.runCommand(new CreateSmartphoneCommand("Smartphone1"));
		// devicesAppState.runCommand(new SetDeviceInCoordenatesCommand("Smartphone1", Vector3f.UNIT_Y));
		
		// Ubication of smartphone on de body
		devicesAppState.runCommand(new SetDeviceOnPartOfBodyCommand("Patient", "Smartphone1",
				SetDeviceOnPartOfBodyCommand.PartOfBody.Chest));

		/**
		 * devicesAppState.runCommand(new
		 * SetAndroidEmulatorCommand("Smartphone1", "Smartphone1",
		 * "emulator-5554")); devicesAppState.runCommand(new
		 * StartActivityCommand("Smartphone1", "phat.android.apps",
		 * "BodyPositionMonitoring"));
		 * 
		 * DisplayAVDScreenCommand displayCommand = new
		 * DisplayAVDScreenCommand("Smartphone1", "Smartphone1");
		 * displayCommand.setFrecuency(0.5f);
		 * devicesAppState.runCommand(displayCommand);
		 */
		
		// Positions of sensor Acelerómeters
		devicesAppState.runCommand(new CreateAccelerometerSensorCommand("sensor1"));
		devicesAppState.runCommand(new SetDeviceOnPartOfBodyCommand("Patient", "sensor1",
				SetDeviceOnPartOfBodyCommand.PartOfBody.LeftHand));

		devicesAppState.runCommand(new CreateAccelerometerSensorCommand("sensor2"));
		devicesAppState.runCommand(new SetDeviceOnPartOfBodyCommand("Patient", "sensor2",
				SetDeviceOnPartOfBodyCommand.PartOfBody.RightHand));

		serverAppState = new ServerAppState();
		stateManager.attach(serverAppState);
		
		// Activate Acelerómeters
		serverAppState.runCommand(new ActivateAccelerometerServerCommand("PatientBodyAccel", "sensor1"));
		serverAppState.runCommand(new ActivateAccelerometerServerCommand("PatientBodyAccel", "sensor2"));

		stateManager.attach(new AbstractAppState() {
			PHATApplication app;

			@Override
			public void initialize(AppStateManager asm, Application aplctn) {
				app = (PHATApplication) aplctn;

			}

			float cont = 0f;
			boolean fall = false;
			float timeToFall = 7f;
			boolean init = false;
			boolean traindata = false;
			Random r = new Random();
			int anim = 0;

			@Override
			public void update(float f) {
				if (!init && !traindata) {
					// Grafica del Sensor2
					AccelerometerControl ac2 = devicesAppState.getDevice("sensor2")
							.getControl(AccelerometerControl.class);
					ac2.setMode(AccelerometerControl.AMode.GRAVITY_MODE);
					XYAccelerationsChart chart2 = new XYAccelerationsChart("Data Accelerations Rigth Hand",
							"Local Sensor PHAT-SIM Rigth Hand", "m/s2", "x,y,z");
					ac2.add(chart2);
					chart2.showWindow();
					init = true;
					
					// Grafica del Sensor1
					AccelerometerControl ac1 = devicesAppState.getDevice("sensor1")
							.getControl(AccelerometerControl.class);
					ac1.setMode(AccelerometerControl.AMode.GRAVITY_MODE);
					XYAccelerationsChart chart1 = new XYAccelerationsChart("Data Accelerations Left Hand",
							"Local Sensor PHAT-SIM Left Hand", "m/s2", "x,y,z");
					ac1.add(chart1);
					chart1.showWindow();
					init = true;
					
					// Grafica del Sensor...

				}

				cont += f;
				if (cont > timeToFall && cont < timeToFall + 1 && !fall) {
					int valor = r.nextInt((10 - 1) + 1) + 1;
					if (anim == 0) {
						bodiesAppState.runCommand(new PlayBodyAnimationCommand("Patient", "DrinkStanding"));
						ss.setStatus("DrinkStanding");
						anim = 3;
					} 
					if (anim == 1) {
						bodiesAppState.runCommand(new PlayBodyAnimationCommand("Patient", "WalkForward"));
						ss.setStatus("WalkForward");
						anim = 2;
					}
					fall = true;
				} else if (fall && cont > timeToFall + 7) {
					if (anim == 2) {
						bodiesAppState.runCommand(new PlayBodyAnimationCommand("Patient", "RunForward"));
						ss.setStatus("ScratchArm");
						anim = 0;
					}
					if (anim == 3){
						bodiesAppState.runCommand(new PlayBodyAnimationCommand("Patient", "WaveAttention"));
						ss.setStatus("WaveAttention");
						anim = 1;
					}

					
					fall = false;
					cont = 0;
				} else {
					ss.setStatus("Stop");
				}
			}
		});
	}
}