package phat.demos.test;

import java.util.logging.Logger;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.system.AppSettings;

import phat.app.PHATApplication;
import phat.app.PHATInitAppListener;
import phat.body.BodiesAppState;
import phat.body.commands.PlayBodyAnimationCommand;
import phat.body.commands.SetBodyInCoordenatesCommand;
import phat.body.commands.SetCameraToBodyCommand;
import phat.body.commands.SetSpeedDisplacemenetCommand;
import phat.body.commands.SetStoopedBodyCommand;
import phat.devices.DevicesAppState;
import phat.devices.commands.CreateAccelerometerSensorCommand;
import phat.devices.commands.SetDeviceOnPartOfBodyCommand;
import phat.sensors.accelerometer.AccelerometerControl;
import phat.sensors.accelerometer.XYAccelerationsChart;
import phat.server.ServerAppState;
import phat.server.commands.ActivateAccelerometerServerCommand;
import phat.structures.houses.TestHouse;
import phat.util.Debug;
import phat.util.SpatialFactory;
import phat.world.WorldAppState;

/**
 *
 * @author pablo
 */
public class AnimWorldDemoClassificationTest implements PHATInitAppListener {

	private static final Logger logger = Logger.getLogger(TestHouse.class.getName());
	BodiesAppState bodiesAppState;
	ServerAppState serverAppState;
	DevicesAppState devicesAppState;
	WorldAppState worldAppState;

	public static void main(String[] args) {
		AnimWorldDemoClassificationTest test = new AnimWorldDemoClassificationTest();
		PHATApplication phat = new PHATApplication(test);
		phat.setDisplayFps(true);
		phat.setDisplayStatView(false);
		AppSettings settings = new AppSettings(true);
		settings.setTitle("PHAT");
		settings.setWidth(640);
		settings.setHeight(480);
		phat.setSettings(settings);
		phat.start();
	}

	@Override
	public void init(SimpleApplication app) {
		SpatialFactory.init(app.getAssetManager(), app.getRootNode());

		AppStateManager stateManager = app.getStateManager();

		app.getFlyByCamera().setMoveSpeed(10f);

		app.getCamera().setLocation(new Vector3f(0.2599395f, 2.7232018f, 3.373138f));
		app.getCamera().setRotation(new Quaternion(-0.0035931943f, 0.9672268f, -0.25351822f, -0.013704466f));

		BulletAppState bulletAppState = new BulletAppState();
		bulletAppState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
		stateManager.attach(bulletAppState);
		bulletAppState.getPhysicsSpace().setAccuracy(1 / 60f);
		
		worldAppState = new WorldAppState();
		worldAppState.setLandType(WorldAppState.LandType.Grass);
		app.getStateManager().attach(worldAppState);
		worldAppState.setCalendar(2013, 1, 1, 12, 0, 0);

		Debug.enableDebugGrid(10, app.getAssetManager(), app.getRootNode());
		bodiesAppState = new BodiesAppState();
		stateManager.attach(bodiesAppState);

		bodiesAppState.createBody(BodiesAppState.BodyType.Elder, "Patient");
		bodiesAppState.runCommand(new SetBodyInCoordenatesCommand("Patient", Vector3f.ZERO));
		
		bodiesAppState.runCommand(new SetSpeedDisplacemenetCommand("Patient", 0.5f));
		bodiesAppState.runCommand(new SetStoopedBodyCommand("Patient", true));

		SetCameraToBodyCommand camCommand = new SetCameraToBodyCommand("Patient");
		camCommand.setDistance(3);
		camCommand.setFront(true);
		bodiesAppState.runCommand(camCommand);

		devicesAppState = new DevicesAppState();
		stateManager.attach(devicesAppState);

		devicesAppState.runCommand(new CreateAccelerometerSensorCommand("sensor1"));
		devicesAppState.runCommand(
				new SetDeviceOnPartOfBodyCommand("Patient", "sensor1", SetDeviceOnPartOfBodyCommand.PartOfBody.Chest));

		devicesAppState.runCommand(new CreateAccelerometerSensorCommand("sensor2"));
		devicesAppState.runCommand(new SetDeviceOnPartOfBodyCommand("Patient", "sensor2",
				SetDeviceOnPartOfBodyCommand.PartOfBody.RightHand));

		serverAppState = new ServerAppState();
		stateManager.attach(serverAppState);

		serverAppState.runCommand(new ActivateAccelerometerServerCommand("PatientBodyAccel", "sensor1"));
		serverAppState.runCommand(new ActivateAccelerometerServerCommand("PatientBodyAccel", "sensor2"));

		stateManager.attach(new AbstractAppState() {
			PHATApplication app;

			@Override
			public void initialize(AppStateManager asm, Application aplctn) {
				app = (PHATApplication) aplctn;

			}

			float cont = 0f;
			boolean fall = false;
			float timeToChange = 7f;
			boolean init = false;
			
			@Override
			public void update(float f) {
				if (!init) {
					AccelerometerControl ac = devicesAppState.getDevice("sensor2")
							.getControl(AccelerometerControl.class);
					ac.setMode(AccelerometerControl.AMode.GRAVITY_MODE);
					XYAccelerationsChart chart = new XYAccelerationsChart("Chart - Acc.", "Local accelerations", "m/s2",
							"x,y,z");
					ac.add(chart);
					chart.showWindow();
					init = true;

				}

				 cont += f;
					if (cont > timeToChange && cont < timeToChange + 1f && !fall) { 
						System.out.println("Change to DrinkStanding:::" + String.valueOf(cont) + "-" + String.valueOf(f));
						bodiesAppState.runCommand(new PlayBodyAnimationCommand("Patient", "DrinkStanding"));
						/** 
						 * Gesture
						 * SpinSpindle: 	abrir puerta con dificultad
						 * Hands2Hips: 		llevar manos a la cadera, (dolor de espalda)
						 * Hand2Belly: 		llevar la mano al vientre, (dolor de vientre)
						 * Wave: 			pedir ayuda o llamar atención
						 * ScratchArm: 		rascar el codo
						 * LeverPole: 		molestias en el movimiento y pedir ayuda
						 * 
						 */
						fall = true;
					} else {
						if (fall && cont > timeToChange + 7f) {
							System.out.println("Change to WaveAttention:::" + String.valueOf(cont) + "-" + String.valueOf(f));
							bodiesAppState.runCommand(new PlayBodyAnimationCommand("Patient", "WaveAttention"));
							fall = false;
							cont = 0;
						}
					}	
			}
		});

	}

}

