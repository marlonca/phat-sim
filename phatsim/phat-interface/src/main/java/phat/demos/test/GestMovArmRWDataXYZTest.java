/*
 * Copyright (C) 2014 Pablo Campillo-Sanchez <pabcampi@ucm.es>
 *
 * This software has been developed as part of the 
 * SociAAL project directed by Jorge J. Gomez Sanz
 * (http://grasia.fdi.ucm.es/sociaal)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package phat.demos.test;

import java.util.logging.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

import phat.app.PHATApplication;
import phat.app.PHATInitAppListener;
import phat.body.BodiesAppState;
import phat.body.commands.MovArmCommand;
import phat.body.commands.OpenObjectCommand;
import phat.commands.PHATCommand;
import phat.commands.PHATCommandListener;
import phat.devices.DevicesAppState;
import phat.devices.commands.CreateSmartphoneCommand;
import phat.devices.commands.SetDeviceOnFurnitureCommand;
import phat.devices.commands.SetDeviceOnPartOfBodyCommand;
import phat.environment.SpatialEnvironmentAPI;
import phat.sensors.accelerometer.AccelerometerControl;
import phat.sensors.accelerometer.XYAccelerationsChart;
import phat.server.ServerAppState;
import phat.structures.houses.HouseFactory;
import phat.structures.houses.commands.CreateHouseCommand;
import phat.world.WorldAppState;

/**
 * Class example Test gest Move Arm Read Write Data axis X, Y and Z from simulations.
 * @author UCM
 */
public class GestMovArmRWDataXYZTest implements PHATInitAppListener {

    private static final Logger logger = Logger.getLogger(GestMovArmRWDataXYZTest.class.getName());
    
    BodiesAppState bodiesAppState;
    DevicesAppState devicesAppState;
    WorldAppState worldAppState;
    ServerAppState serverAppState;

    @Override
    public void init(SimpleApplication app) {
    	logger.info("init");
        AppStateManager stateManager = app.getStateManager();
        
        app.getFlyByCamera().setMoveSpeed(10f);

        BulletAppState bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        bulletAppState.setDebugEnabled(false);

        SpatialEnvironmentAPI seAPI = SpatialEnvironmentAPI.createSpatialEnvironmentAPI(app);

        seAPI.getWorldAppState().setCalendar(2016, 2 ,18, 12, 30, 0);
        seAPI.getHouseAppState().runCommand(new CreateHouseCommand("House1", HouseFactory.HouseType.BrickHouse60m));

        bodiesAppState = new BodiesAppState();
        stateManager.attach(bodiesAppState);
        openObject("Patient","Fridge1");
        
        //Se crean los personajes
        bodiesAppState.createBody(BodiesAppState.BodyType.Elder, "Patient");
        //Se posicionan en la casa
        bodiesAppState.setInSpace("Patient", "House1", "LivingRoom");
        
        devicesAppState = new DevicesAppState();
        stateManager.attach(devicesAppState);
        
        devicesAppState.runCommand(new CreateSmartphoneCommand("SmartWatch1").setDimensions(0.03f, 0.03f, 0.01f));
        devicesAppState.runCommand(new SetDeviceOnPartOfBodyCommand("Patient", "SmartWatch1", 
                SetDeviceOnPartOfBodyCommand.PartOfBody.LeftWrist));
        
        devicesAppState.runCommand(new SetDeviceOnFurnitureCommand("deviceId", "House1", "furnitureId"));
        
        stateManager.attach(new AbstractAppState() {

        	@Override
            public void initialize(AppStateManager asm, Application aplctn) {}
            
            boolean init = false;

            @Override
            public void update(float f) {
            	if(!init){
            		AccelerometerControl ac = devicesAppState.getDevice("SmartWatch1").getControl(AccelerometerControl.class);
                    ac.setMode(AccelerometerControl.AMode.ACCELEROMETER_MODE);
                    XYAccelerationsChart chart = new XYAccelerationsChart("Chart - Accelerometer Mode", "SmartWatch1 Mov", "---", "x,y,z");
                    ac.add(chart);
                    chart.showWindow();
                    init = true;	
            	}
            }
        });
        
        gestOpenDoor("Patient");
        
        app.getCamera().setLocation(new Vector3f(7f, 7.25f, 3.1f));
        app.getCamera().setRotation(new Quaternion(0.37133554f, -0.6016627f, 0.37115145f, 0.60196227f));
    }
    
    /**
     * Gest hand mov to open door.
     * @param idPersont person or patient to actions
     * @param door object to close
     */
    private void gestOpenDoor(final String idPersont) {
    	logger.info("goToClose: " + idPersont);
    	MovArmCommand gtc = new MovArmCommand(idPersont, true, MovArmCommand.LEFT_ARM, new PHATCommandListener() {
            @Override
            public void commandStateChanged(PHATCommand command) {
                if (command.getState() == PHATCommand.State.Success) {

                }
            }
        });
        bodiesAppState.runCommand(gtc);
    }
    
    /**
     * Open Door Actions.
     * @param idPersont
     * @param door 
     */
    private void openObject(final String idPersont, final String door) {
        OpenObjectCommand gtc = new OpenObjectCommand(idPersont, door);
        bodiesAppState.runCommand(gtc);
    }
    
    /**
     * Main Class, executios Test.
     * @param args 
     */
    public static void main(String[] args) {
        GestMovArmRWDataXYZTest app = new GestMovArmRWDataXYZTest();
        
        PHATApplication phat = new PHATApplication(app);
        phat.setDisplayFps(false);
        phat.setDisplayStatView(false);
        phat.setShowSettings(false);
        
        phat.start();
    }
    
    private void generateClassifications(){
    	ScriptEngineManager manager = new ScriptEngineManager();
    	ScriptEngine engine = manager.getEngineByName("Renjin");
    	
    	if(engine == null){
    		throw new RuntimeException("No es posible cargar el ejecutor de R");
    	}
    	
    	try{
    		engine.eval("df <- data.frame(x=10, y=(1:10) + rnorm(n=10))");
    		   		
    		engine.eval("print(df)");
    		engine.eval("print(lm(y ~ x, df))");
    	}catch(ScriptException e){
    		e.printStackTrace();
    	}
    }
}