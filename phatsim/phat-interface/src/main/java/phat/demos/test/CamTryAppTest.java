///**
// * 
// */
//package phat.demos.test;
//
//import com.jme3.app.SimpleApplication;
//import com.jme3.bounding.BoundingSphere;
//import com.jme3.math.Vector3f;
//import com.jme3.renderer.Camera;
//import com.jme3.scene.Node;
//import com.jme3.scene.shape.Box;
//
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// * @author mcardenas
// *
// */
//public class CamTryAppTest extends SimpleApplication {
//
//	protected Camera cam;
//	protected Node rootNode;
//	protected InputHandler input;
//	protected Timer timer;
//	protected float tpf;
//	protected boolean showBounds = false;
//	protected WireframeState wireState;
//	protected LightState lightState;
//
//	private static final Logger logger = Logger.getLogger(CamTryAppTest.class.getName());
//
//	public static void main(String[] args) {
//		CamTryAppTest app = new CamTryAppTest();
//		app.setDialogBehaviour(AbstractA.ALWAYS_SHOW_PROPS_DIALOG);
//		app.start();
//	}
//
//	protected final void update(float interpolation) {
//		// Recarga el ratio de frames
//		timer.update();
//		// Actualiza tiempo por frame
//		tpf = timer.getTimePerFrame();
//		// Comprueba actualizaciones del raton/teclado
//		input.update(tpf);
//
//		// Se llama siempre a este metodo en cualquier clase derivada
//		// de SimpleGame
//		simpleUpdate();
//
//		// Actualiza controladores/estado del render/transformaciones y bounds
//		rootNode.updateGeometricState(tpf, true);
//
//		if (KeyBindingManager.getKeyBindingManager().isValidCommand("exit", false)) {
//			finish();
//		}
//
//		if (KeyBindingManager.getKeyBindingManager().isValidCommand("perspectiva1", false)) {
//			cameraPerspective1();
//		}
//
//		if (KeyBindingManager.getKeyBindingManager().isValidCommand("perspectiva2", false)) {
//			cameraPerspective2();
//		}
//
//		if (KeyBindingManager.getKeyBindingManager().isValidCommand("perspectiva3", false)) {
//			cameraPerspective3();
//		}
//
//	}
//
//	protected final void render(float interpolation) {
//		display.getRenderer().clearStatistics();
//		display.getRenderer().clearBuffers();
//		// Dibuja el nodo principal y todos sus hijos
//		display.getRenderer().draw(rootNode);
//		// Si hay bounds, dibuja los bounds
//		if (showBounds)
//			Debugger.drawBounds(rootNode, display.getRenderer());
//
//		// Se llama a este metodo en todas las clases derivadas
//		simpleRender();
//	}
//
//	protected final void initSystem() {
//		try {
//			display = DisplaySystem.getDisplaySystem(properties.getRenderer());
//			display.createWindow(properties.getWidth(), properties.getHeight(), properties.getDepth(),
//					properties.getFreq(), properties.getFullscreen());
//			// Crea una camara especifica para el display
//			cam = display.getRenderer().createCamera(display.getWidth(), display.getHeight());
//
//		} catch (JmeException e) {
//			logger.log(Level.SEVERE, "Could not create displaySystem", e);
//			System.exit(1);
//		}
//
//		display.getRenderer().setBackgroundColor(ColorRGBA.black);
//
//		// Establece la perspectiva de la camara
//		cameraPerspective1();
//		Vector3f loc = new Vector3f(0.0f, 0.0f, 50.0f);
//		Vector3f left = new Vector3f(-1.0f, 0.0f, 0.0f);
//		Vector3f up = new Vector3f(0.0f, 1.0f, 0.0f);
//		Vector3f dir = new Vector3f(0.0f, 0f, -1.0f);
//		// Mueve la camara al plano correcto
//		cam.setFrame(loc, left, up, dir);
//		// Actualiza
//		cam.update();
//		// Asigna la camara al render
//		display.getRenderer().setCamera(cam);
//
//		// Crea un controlador basico de primera persona
//		FirstPersonHandler firstPersonHandler = new FirstPersonHandler(cam, 20, 1);
//		input = firstPersonHandler;
//
//		timer = Timer.getTimer();
//
//		display.setTitle("Ejemplo Camaras");
//		KeyBindingManager.getKeyBindingManager().set("exit", KeyInput.KEY_ESCAPE);
//
//		KeyBindingManager.getKeyBindingManager().set("perspectiva1", KeyInput.KEY_I);
//		KeyBindingManager.getKeyBindingManager().set("perspectiva2", KeyInput.KEY_O);
//		KeyBindingManager.getKeyBindingManager().set("perspectiva3", KeyInput.KEY_P);
//	}
//
//	protected void cameraPerspective1() {
//		cam.setFrustumPerspective(45.0f, (float) display.getWidth() / (float) display.getHeight(), 1, 1000);
//		cam.setParallelProjection(false);
//		cam.update();
//	}
//
//	protected void cameraPerspective2() {
//		cam.setFrustumPerspective(85.0f, (float) display.getWidth() / (float) display.getHeight(), 1, 1000);
//		cam.setParallelProjection(false);
//		cam.update();
//	}
//
//	protected void cameraPerspective3() {
//		cam.setParallelProjection(true);
//		float aspect = (float) display.getWidth() / display.getHeight();
//		cam.setFrustum(-100, 1000, -50 * aspect, 50 * aspect, -50, 50);
//		cam.update();
//	}
//
//	protected final void initGame() {
//		rootNode = new Node("rootNode");
//
//		/**
//		 * Create a wirestate to toggle on and off. Starts disabled with default
//		 * width of 1 pixel.
//		 */
//		wireState = display.getRenderer().createWireframeState();
//		wireState.setEnabled(false);
//		rootNode.setRenderState(wireState);
//
//		/**
//		 * Create a ZBuffer to display pixels closest to the camera above
//		 * farther ones.
//		 */
//		ZBufferState buf = display.getRenderer().createZBufferState();
//		buf.setEnabled(true);
//		buf.setFunction(ZBufferState.CF_LEQUAL);
//
//		rootNode.setRenderState(buf);
//
//		// -- FPS DISPLAY
//		// First setup alpha state
//		/**
//		 * This allows correct blending of text and what is already rendered
//		 * below it
//		 */
//		AlphaState as1 = display.getRenderer().createAlphaState();
//		as1.setBlendEnabled(true);
//		as1.setSrcFunction(AlphaState.SB_SRC_ALPHA);
//		as1.setDstFunction(AlphaState.DB_ONE);
//		as1.setTestEnabled(true);
//		as1.setTestFunction(AlphaState.TF_GREATER);
//		as1.setEnabled(true);
//
//		// ---- LIGHTS
//		/** Set up a basic, default light. */
//		PointLight light = new PointLight();
//		light.setDiffuse(new ColorRGBA(1.0f, 1.0f, 1.0f, 1.0f));
//		light.setAmbient(new ColorRGBA(0.5f, 0.5f, 0.5f, 1.0f));
//		light.setLocation(new Vector3f(100, 100, 100));
//		light.setEnabled(true);
//
//		/** Attach the light to a lightState and the lightState to rootNode. */
//		lightState = display.getRenderer().createLightState();
//		lightState.setEnabled(true);
//		lightState.attach(light);
//		rootNode.setRenderState(lightState);
//
//		/** Let derived classes initialize. */
//		simpleInitGame();
//
//		/**
//		 * Update geometric and rendering information for both the rootNode and
//		 * fpsNode.
//		 */
//		rootNode.updateGeometricState(0.0f, true);
//		rootNode.updateRenderState();
//
//	}
//
//	@Override
//	public void simpleInitApp() {
//		Vector3f max = new Vector3f(20, 10, 10);
//		Vector3f min = new Vector3f(0, 0, 0);
//
//		Box t = new Box("Box", min, max);
//		t.setModelBound(new BoundingSphere());
//		t.updateModelBound();
//
//		Pyramid t2 = new Pyramid("Pyramid", 15, 20);
//		t2.setModelBound(new BoundingSphere());
//		t2.updateModelBound();
//
//		t.setLocalTranslation(new Vector3f(0, 0, -10));
//		ColorRGBA verde = new ColorRGBA(0, 5, 0, 0);
//
//		t.setSolidColor(verde);
//		t2.setLocalTranslation(new Vector3f(-20, 0, 0));
//		rootNode.attachChild(t);
//		rootNode.attachChild(t2);
//	}
//
//	protected void simpleUpdate() {
//	}
//
//	protected void simpleRender() {
//	}
//
//	protected void reinit() {
//	}
//
//	protected void cleanup() {
//		logger.info("Cleaning up resources.");
//		KeyInput.destroyIfInitalized();
//		MouseInput.destroyIfInitalized();
//		JoystickInput.destroyIfInitalized();
//	}
//
//}
