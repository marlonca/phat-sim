/*
 * Copyright (C) 2014 Pablo Campillo-Sanchez <pabcampi@ucm.es>
 *
 * This software has been developed as part of the 
 * SociAAL project directed by Jorge J. Gomez Sanz
 * (http://grasia.fdi.ucm.es/sociaal)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package phat.demos.test;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

import phat.environment.*;

import java.util.logging.Logger;

import phat.app.PHATApplication;
import phat.app.PHATInitAppListener;
import phat.body.BodiesAppState;
import phat.body.BodiesAppState.BodyType;
import phat.body.commands.AlignWithCommand;
import phat.body.commands.CloseObjectCommand;
import phat.body.commands.GoCloseToObjectCommand;
import phat.body.commands.MovArmCommand;
import phat.body.commands.OpenObjectCommand;
import phat.commands.PHATCommand;
import phat.commands.PHATCommandListener;
import phat.structures.houses.HouseFactory;
import phat.structures.houses.commands.CreateHouseCommand;

/**
 * Class example Test rum simulatios.
 * @author UCM
 */
public class BodiesStateMCTest implements PHATInitAppListener {

    private static final Logger logger = Logger.getLogger(BodiesStateMCTest.class.getName());
    
    BodiesAppState bodiesAppState;

    @Override
    public void init(SimpleApplication app) {
    	logger.info("init");
        AppStateManager stateManager = app.getStateManager();
        
        app.getFlyByCamera().setMoveSpeed(10f);

        BulletAppState bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        bulletAppState.setDebugEnabled(false);

        SpatialEnvironmentAPI seAPI = SpatialEnvironmentAPI.createSpatialEnvironmentAPI(app);

        seAPI.getWorldAppState().setCalendar(2016, 2 ,18, 12, 30, 0);
        seAPI.getHouseAppState().runCommand(new CreateHouseCommand("House1", HouseFactory.HouseType.BrickHouse60m));

        bodiesAppState = new BodiesAppState();
        stateManager.attach(bodiesAppState);
        openObject("Patient","Fridge1");
        
        //Se crean los personajes
        bodiesAppState.createBody(BodiesAppState.BodyType.Elder, "Patient");
        
        //Se crean los personajes
        bodiesAppState.createBody(BodiesAppState.BodyType.Young, "Patient2");
        
        //Se posicionan en la casa
        bodiesAppState.setInSpace("Patient", "House1", "LivingRoom");
        
        bodiesAppState.setInSpace("Patient2", "House1", "LivingRoom");
        
        //bodiesAppState.runCommand(new MovArmCommand("Patient", true, MovArmCommand.LEFT_ARM));
        
        bodiesAppState.runCommand(new MovArmCommand("Patient2", true, MovArmCommand.LEFT_ARM));
        
        bodiesAppState.runCommand(new MovArmCommand("Patient", true, MovArmCommand.LEFT_ARM));
        
        goCloseToObject("Patient2", "Fridge1");
        
        //bodiesAppState.runCommand(new MovArmCommand("Patient", true, MovArmCommand.RIGHT_ARM));
        
        //app.getCamera().setLocation(new Vector3f(9.692924f, 11.128746f, 4.5429335f));
        app.getCamera().setLocation(new Vector3f(7f, 7.25f, 3.1f));
        app.getCamera().setRotation(new Quaternion(0.37133554f, -0.6016627f, 0.37115145f, 0.60196227f));
    }
    
    /**
     * Go Up Hand
     * @param idPersont person or patient to actions
     * @param door object to close
     */
    private void goCloseToObject(final String idPersont, final String door) {
    	logger.info("goToClose: " + idPersont + ", object: " + door);
        GoCloseToObjectCommand gtc = new GoCloseToObjectCommand(idPersont, door, new PHATCommandListener() {
            @Override
            public void commandStateChanged(PHATCommand command) {
                if (command.getState() == PHATCommand.State.Success) {
                	bodiesAppState.runCommand(new MovArmCommand(idPersont, false, MovArmCommand.LEFT_ARM));
                    bodiesAppState.runCommand(new AlignWithCommand(idPersont, door));
                    bodiesAppState.runCommand(new CloseObjectCommand(idPersont, door));
                    bodiesAppState.runCommand(new MovArmCommand(idPersont,true, MovArmCommand.LEFT_ARM));
                }
            }
        });
        gtc.setMinDistance(0.1f);
        bodiesAppState.runCommand(gtc);
    }
    
    /**
     * Open Door Actions.
     * @param idPersont
     * @param door 
     */
    private void openObject(final String idPersont, final String door) {
        OpenObjectCommand gtc = new OpenObjectCommand(idPersont, door);
        bodiesAppState.runCommand(gtc);
    }
    
    /**
     * Main Class, executios Test.
     * @param args 
     */
    public static void main(String[] args) {
        BodiesStateMCTest app = new BodiesStateMCTest();
        
        PHATApplication phat = new PHATApplication(app);
        phat.setDisplayFps(false);
        phat.setDisplayStatView(false);
        phat.setShowSettings(false);
        
        phat.start();
    }
}